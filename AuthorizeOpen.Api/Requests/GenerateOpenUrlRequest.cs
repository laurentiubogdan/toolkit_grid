﻿namespace AuthorizeOpen.Api
{
    public class GenerateOpenUrlRequest
    {
        public string AuthorizationToken { get; set; }
        public GenerateOpenUrlBody Payload { get; set; } 
    }
}
