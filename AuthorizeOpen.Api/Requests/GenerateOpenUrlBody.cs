﻿namespace AuthorizeOpen.Api
{
    public class GenerateOpenUrlBody
    {
        public UserProfile User { get; set; }
        public string ReturnUrl { get; set; }
    }
}
