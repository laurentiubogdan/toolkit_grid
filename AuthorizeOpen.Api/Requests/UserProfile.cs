﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AuthorizeOpen.Api
{
    public class UserProfile
    {
        public string UniqueID { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string AO { get; set; }
        public string SAO { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
    }
}
