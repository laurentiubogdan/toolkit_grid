﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace AuthorizeOpen.Api
{
    public class AuthorizeOpenApi : IAuthorizeOpenApi
    {
        private readonly AuthorizeOpenConfigurations _authorizeOpenApiConfig;

        public AuthorizeOpenApi(AuthorizeOpenConfigurations authorizeOpenApiConfig)
        {
            _authorizeOpenApiConfig = authorizeOpenApiConfig;
        }

        public async Task<GenerateOpenUrlResponse> GenerateOpenUrlAsync(GenerateOpenUrlRequest generateOpenUrlRequest)
        {
            string url = $"{_authorizeOpenApiConfig.ApiUrl}/authorise";

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorisation", $"Bearer {generateOpenUrlRequest.AuthorizationToken}");
                StringContent content = new StringContent(JsonConvert.SerializeObject(generateOpenUrlRequest.Payload));
                HttpResponseMessage httpResponseMessage = await client.PostAsync(url, content);
                if (httpResponseMessage.IsSuccessStatusCode == false)
                {
                    throw new Exception($"Failed to generate authorized open url. {httpResponseMessage.ReasonPhrase}");
                }

                string urlResponse = await httpResponseMessage.Content.ReadAsStringAsync();

                GenerateOpenUrlResponse generateOpenUrlResponse = new GenerateOpenUrlResponse
                {
                    Url = urlResponse
                };

                return generateOpenUrlResponse;
            }
        }
    }
}
