﻿namespace AuthorizeOpen.Api
{
    public class GenerateOpenUrlResponse
    {
        public string Url { get; set; }
    }
}
