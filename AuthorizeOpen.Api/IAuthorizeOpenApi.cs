﻿using System.Threading.Tasks;

namespace AuthorizeOpen.Api
{
    public interface IAuthorizeOpenApi
    {
        Task<GenerateOpenUrlResponse> GenerateOpenUrlAsync(GenerateOpenUrlRequest generateOpenUrlRequest);
    }
}