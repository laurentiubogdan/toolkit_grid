﻿using System.ComponentModel.DataAnnotations;

namespace ToolKitGrid.Domain.HelperModels
{
    public class Employer
    {
        [Required]
        public string CompanyName { get; set; }
        [Required]
        public string BusinessStructure { get; set; }
        public Owner Partner { get; set; }
    }
}
