﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ToolKitGrid.Domain.HelperModels
{
    public class UserSession : IMongoPropertiesInterface
    {
        public string Id { get; set; }
        [Required]
        public List<SessionData> SessionData { get; set; }
        [Required]
        public string UserId { get; set; }
        public string Email { get; set; }
    }
}
