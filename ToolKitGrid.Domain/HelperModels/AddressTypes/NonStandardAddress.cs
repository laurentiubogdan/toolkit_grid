﻿using System.ComponentModel.DataAnnotations;

namespace ToolKitGrid.Domain.HelperModels.AddressTypes
{
    public class NonStandardAddress : IMongoPropertiesInterface
    {
        [Required]
        public string Line1 { get; set; }
        public string Line2 { get; set; }
    }
}
