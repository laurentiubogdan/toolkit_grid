﻿using System.ComponentModel.DataAnnotations;

namespace ToolKitGrid.Domain.HelperModels
{
    public class E_Spouse : IMongoPropertiesInterface
    {
        [Required]
        public string NameTitle { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string Surname { get; set; }
    }
}
