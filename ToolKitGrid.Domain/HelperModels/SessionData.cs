﻿using System;
using ToolKitGrid.Domain.Models.Identity;

namespace ToolKitGrid.Domain.HelperModels
{
    public class SessionData : IMongoPropertiesInterface
    {
        public string SessionId { get; set; }
        public LoanWriter User { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}
