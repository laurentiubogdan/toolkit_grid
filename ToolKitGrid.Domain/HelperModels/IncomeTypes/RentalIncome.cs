﻿using System.ComponentModel.DataAnnotations;
using ToolKitGrid.Domain.HelperModels.AddressTypes;

namespace ToolKitGrid.Domain.HelperModels.IncomeTypes
{
    public class RentalIncome : IMongoPropertiesInterface
    {
        public string Id { get; set; }
        [Required]
        public string Transaction { get; set; }
        [Required]
        public string PrimaryUsage { get; set; }
        [Required]
        public string PrimaryPurpose { get; set; }
        public string Status { get; set; }
        public string ApprovalInPrinciple { get; set; }
        public float RentalAmount { get; set; }
        public string RentalFrequency { get; set; }
        public Address Address { get; set; }
        public Owner Owner { get; set; }
    }
}
