﻿using System.ComponentModel.DataAnnotations;

namespace ToolKitGrid.Domain.HelperModels.IncomeTypes
{
    public class PAYG : IMongoPropertiesInterface
    {
        [Required]
        public string Id { get; set; }
        [Required]
        public string Status { get; set; }
        [Required]
        public string Basis { get; set; }
        [Required]
        public string OccupationCode { get; set; }
        [Required]
        public string StartDate { get; set; }
        public string OnProbation { get; set; }
        public string CompanyCar { get; set; }
        [Required]
        public string IndustryCode { get; set; }
        public int DurationLength { get; set; }
        public string DurationUnits { get; set; }
        [Required]
        public float GrossSalaryAmount { get; set; }
        [Required]
        public string GrossSalaryFrequency { get; set; }
        [Required]
        public float NetSalaryAmount { get; set; }
        [Required]
        public string NetSalaryFrequency { get; set; }
        public float GrossRegularOvertimeAmount { get; set; }
        public string GrossRegularOvertimeFrequency { get; set; }
        public float BonusAmount { get; set; }
        public string BonusFrequency { get; set; }
        public float CommissionAmount { get; set; }
        public string CommissionFrequency { get; set; }
        public float CarAllowanceAmount { get; set; }
        public string CarAllowanceFrequency { get; set; }
        public float WorkAllowanceAmount { get; set; }
        public string WorkAllowanceFrequency { get; set; }
        public float WorkersCompensationAmount { get; set; }
        public string WorkersCompensationFrequency { get; set; }
        public bool FullyMaintainedCompanyCar { get; set; }
        public Owner Owner { get; set; }
    }
}
