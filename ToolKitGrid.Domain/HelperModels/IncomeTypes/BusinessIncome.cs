﻿using System.ComponentModel.DataAnnotations;

namespace ToolKitGrid.Domain.HelperModels.IncomeTypes
{
    public class BusinessIncome : IMongoPropertiesInterface
    {
        [Required]
        public string StartDate { get; set; }
        [Required]
        public string EndDate { get; set; }
        [Required]
        public float ProfitBeforeTax { get; set; }
        [Required]
        public float Deprecation { get; set; }
        public string Interest { get; set; }
        public float Lease { get; set; }
        public float NonCashBenefits { get; set; }
        public float NonRecurringExpenses { get; set; }
        public float SuperannuationExcess { get; set; }
        public float CarryForwardLosses { get; set; }
        public float AmortisationOfGoodwill { get; set; }
        public float Salary { get; set; }
        public float Allowances { get; set; }
        public float Bonus { get; set; }
        public float CarExpense { get; set; }
    }
}
