﻿using System.ComponentModel.DataAnnotations;

namespace ToolKitGrid.Domain.HelperModels.IncomeTypes
{
    public class SelfEmployed : IMongoPropertiesInterface
    {
        public string Id { get; set; }
        [Required]
        public string Status { get; set; }
        [Required]
        public string Basis { get; set; }
        [Required]
        public string IndustryCode { get; set; }
        [Required]
        public int DurationLength { get; set; }
        [Required]
        public string DurationUnits { get; set; }
        public string OccupationCode { get; set; }
        public string StartDate { get; set; }
        public Employer Employer { get; set; }
        public BusinessIncome BusinessIncomeYearToDate { get; set; }
        public BusinessIncome BusinessIncomeRecent { get; set; }
        public BusinessIncome BusinessIncomePrevious { get; set; }
        public BusinessIncome BusinessIncomePrior { get; set; }
        public Owner Owner { get; set; }
    }
}
