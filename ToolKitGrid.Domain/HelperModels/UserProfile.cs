﻿using System.Collections.Generic;

namespace ToolKitGrid.Domain.HelperModels
{
    public class UserProfile: IMongoPropertiesInterface
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public int ItemsPerPage { get; set; }
        public IEnumerable<KendoColumnWidth> Columns { get; set; }
        public IEnumerable<string> VisibleColumns { get; set; }
    }
}
