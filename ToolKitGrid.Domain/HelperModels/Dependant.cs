﻿using System.ComponentModel.DataAnnotations;

namespace ToolKitGrid.Domain.HelperModels
{
    public class Dependant : IMongoPropertiesInterface
    {
        [Required]
        public string Name { get; set; }
        public int Age { get; set; }
        public string DateOfBirth { get; set; }
    }
}
