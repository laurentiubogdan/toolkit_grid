﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToolKitGrid.Domain.HelperModels
{
    public class KendoColumnWidth
    {
        public string Field { get; set; }
        public int Width { get; set; }
    }
}
