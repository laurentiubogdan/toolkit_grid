﻿using Qualify.Domain.Models.Temporary;
using System;
using System.Collections.Generic;

namespace ToolKitGrid.Domain.Models.Temporary
{
    public class Pricing : IMongoPropertiesInterface
    {
        public string Id { get; set; }
        public IEnumerable<Indicator> Indicators { get; set; }
        public DateTime? RequestDate { get; set; }
        public string Result { get; set; }
    }
}
