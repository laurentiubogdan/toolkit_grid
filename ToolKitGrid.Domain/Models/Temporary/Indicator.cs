﻿namespace Qualify.Domain.Models.Temporary
{
    public class Indicator
    {
        public string BackgroundColorHex { get; set; }
        public string BorderColorHex { get; set; }
        public string Title { get; set; }
        public string Value { get; set; }
    }
}
