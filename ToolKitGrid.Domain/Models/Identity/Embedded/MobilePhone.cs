﻿using System.Text;

namespace ToolKitGrid.Domain.Models.Identity
{
    public class MobilePhone
    {
        public string CountryCode { get; set; }
        public string Number { get; set; }
        public string FullNumber
        {
            get
            {
                StringBuilder stringBuilder = new StringBuilder();
                if (!string.IsNullOrEmpty(CountryCode))
                {
                    stringBuilder.Append($"({CountryCode}) ");
                }
                if (!string.IsNullOrEmpty(Number))
                {   
                    stringBuilder.Append($"{Number}");
                }
                return stringBuilder.ToString();
            }
        }
    }
}
