﻿using System.Collections.Generic;
using ToolKitGrid.Domain.HelperModels;
using ToolKitGrid.Domain.HelperModels.IncomeTypes;

namespace ToolKitGrid.Domain.Models.Embedded
{
    public class Income : IMongoPropertiesInterface
    {
        public string UniqueID { get; set; }
        public string IncomeType { get; set; }
        public PAYG PAYG { get; set; }
        public SelfEmployed SelfEmployed { get; set; }
        public NotEmployed NotEmployed { get; set; }
        public OtherIncome OtherIncome { get; set; }
        public RentalIncome RentalIncome { get; set; }
        public IEnumerable<Owner> Owner { get; set; }
    }
}
