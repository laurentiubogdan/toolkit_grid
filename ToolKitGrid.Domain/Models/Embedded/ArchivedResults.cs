﻿using System.Collections.Generic;
using ToolKitGrid.Domain.Models.Temporary;

namespace ToolKitGrid.Domain.Models.Embedded
{
    public class ArchivedResults : IMongoPropertiesInterface
    {
        public IEnumerable<Pricing> Pricing { get; set; }
        public IEnumerable<Umi> Umi { get; set; }
        public string UniqueID { get; set; }
    }
}
