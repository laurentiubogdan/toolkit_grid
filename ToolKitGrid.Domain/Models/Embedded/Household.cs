﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ToolKitGrid.Domain.HelperModels;

namespace ToolKitGrid.Domain.Models.Embedded
{
    public class Household : IMongoPropertiesInterface
    {
        public string UniqueID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int NumberOfAdults { get; set; }
        [Required]
        public int NumberOfDependants { get; set; }
        [Required]
        public IEnumerable<Dependant> Dependant { get; set; }
    }
}
