﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToolKitGrid.Domain.Models
{
    public class Field
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
