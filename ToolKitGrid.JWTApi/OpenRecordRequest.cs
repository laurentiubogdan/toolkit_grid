﻿namespace ToolKitGrid.JWTApi
{
    public class OpenRecordRequest
    {
        public string RecordUniqueId { get; set; }
    }
}
