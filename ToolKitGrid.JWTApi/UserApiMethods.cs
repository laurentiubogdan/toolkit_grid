﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using ToolKitGrid.JWTApi.Helpers;

namespace ToolKitGrid.JWTApi
{
    public class UserApiMethods
    {
        private readonly UriApiMenthodsConfiguration _apiSettingsUrl;
        private readonly HttpClient _client;
        public UserApiMethods(HttpClient client, UriApiMenthodsConfiguration apiSettingsUrl)
        {
            _client = client;
            _apiSettingsUrl = apiSettingsUrl;
        }

        public async Task<SendJwtResult> SendJWTAsync(OpenRecordRequest request)
        {
            string url = $"{_apiSettingsUrl.RecordQualifyUrl}/qualifyRedirect";

            return new SendJwtResult
            {
                Url = $"{_apiSettingsUrl.RecordQualifyUrl}/view/{request.RecordUniqueId}"
            };

            SendJwtResult responseObject = null;

            using (HttpResponseMessage response = await _client.PostAsJsonAsync(url, request.RecordUniqueId))
            {
                if (!response.IsSuccessStatusCode)
                {
                    return null;
                }

                string dataJson = await response.Content.ReadAsStringAsync();
                responseObject = JsonConvert.DeserializeObject<SendJwtResult>(dataJson);
            }

            return responseObject;
        }
    }
}