﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToolKitGrid.Domain.Models;
using ToolKitGrid.Repository;

namespace ToolkitGrid.Repository.Mongo
{
    public class RecordRepository : IRecordRepository
    {
        private readonly MongoUnitOfWork _unitOfWork;

        public RecordRepository(MongoUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IMongoCollection<Record> BaseQuery()
        {
            return _unitOfWork.Records;
        }

        public async Task CreateAsync(Record record)
        {
            await _unitOfWork.Records.InsertOneAsync(record);
        }

        public async Task<IEnumerable<Record>> GetAllAsync(int pageSize, int page)
        {
            int skipNumber = pageSize * (page - 1);
            return await _unitOfWork.Records.Find(Record => true)
                                            .Skip(skipNumber)
                                            .Limit(pageSize)
                                            .ToListAsync();
        }

        public async Task<Record> GetByIdAsync(string id)
        {
            return await _unitOfWork.Records.Find(r => r.Id == id).FirstOrDefaultAsync();
        }

        public async Task<Record> GetByUniqueIdAsync(string id)
        {
            return await _unitOfWork.Records.Find(r => r.UniqueID == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Record>> GetByUserIdAsync(string userId)
        {
            return null;/*await _unitOfWork.Records.Find(r => r.LoanWriter.LoanWriterAccreditationNumber == userId).ToListAsync();*/
        }

        public async Task<Record> UpdateByIdAsync(string uniqueId, Record record)
        {
            await _unitOfWork.Records.ReplaceOneAsync(r => r.UniqueID == uniqueId, record);

            return await GetByIdAsync(record.Id);
        }

        public async Task UpdateManyAsync(IEnumerable<string> ids, bool archive)
        {
            FilterDefinition<Record> filter = Builders<Record>.Filter.In(r => r.Id, ids);

            await _unitOfWork.Records.UpdateManyAsync(filter, Builders<Record>.Update.Set(r => r.Archived, archive));
        }

        public async Task DeleteByIdAsync(string id)
        {
            await _unitOfWork.Records.DeleteOneAsync(r => r.Id == id);
        }

        public async Task DeleteManyAsync(IEnumerable<string> ids)
        {
            FilterDefinition<Record> filter = Builders<Record>.Filter.In(r => r.Id, ids);

            await _unitOfWork.Records.DeleteManyAsync(filter);
        }
    }
}
