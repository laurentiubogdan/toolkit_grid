﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToolKitGrid.Domain.HelperModels;
using ToolKitGrid.Repository;

namespace ToolkitGrid.Repository.Mongo
{
    public class UserSessionRepository : IUserSessionRepository
    {
        private readonly MongoUnitOfWork _unitOfWork;

        public UserSessionRepository(MongoUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task CreateAsync(UserSession userSession)
        {
            await _unitOfWork.UserSession.InsertOneAsync(userSession);
        }

        public async Task<IEnumerable<UserSession>> GetAllAsync()
        {
            return await _unitOfWork.UserSession.Find(UserSession => true).ToListAsync();
        }

        public async Task<UserSession> GetByIdAsync(string sessionId)
        {
            FilterDefinition<UserSession> userFilter = Builders<UserSession>.Filter.Where(p => p.SessionData.Any(f => f.SessionId == sessionId));

            return await _unitOfWork.UserSession.Find(userFilter).FirstOrDefaultAsync();
        }

        public async Task<UserSession> GetByUserIdAsync(string userId)
        {
            return await _unitOfWork.UserSession.Find(s => s.UserId == userId).FirstOrDefaultAsync();
        }

        public async Task<UserSession> UpdateByIdAsync(string id, UserSession session)
        {
            await _unitOfWork.UserSession.ReplaceOneAsync(r => r.Id == id, session);

            return await GetByUserIdAsync(session.UserId);
        }

        public async Task DeleteByIdAsync(string id)
        {
            FilterDefinition<UserSession> userFilter = Builders<UserSession>.Filter.Where(p => p.SessionData.Any(f => f.SessionId == id));

            await _unitOfWork.UserSession.DeleteOneAsync(userFilter);
        }
    }
}
