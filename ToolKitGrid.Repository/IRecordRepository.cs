﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToolKitGrid.Domain.Models;

namespace ToolKitGrid.Repository
{
    public interface IRecordRepository
    {
        IMongoCollection<Record> BaseQuery();
        Task CreateAsync(Record record);
        Task<IEnumerable<Record>> GetAllAsync(int pageSize, int page);
        Task<Record> GetByIdAsync(string id);
        Task<Record> GetByUniqueIdAsync(string id);
        Task UpdateManyAsync(IEnumerable<string> ids, bool archive);
        Task<IEnumerable<Record>> GetByUserIdAsync(string userId);
        Task<Record> UpdateByIdAsync(string id, Record record);
        Task DeleteByIdAsync(string id);
        Task DeleteManyAsync(IEnumerable<string> ids);
    }
}
