﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ToolKitGrid.Domain.HelperModels;

namespace ToolKitGrid.Repository
{
    public interface IUserProfileRepository
    { 
        Task<IEnumerable<UserProfile>> GetAllAsync();
        Task<UserProfile> GetByIdAsync(string userProfileId);
        Task<UserProfile> GetByUserIdAsync(string userId);
        Task<UserProfile> CreateAsync(UserProfile userProfile);
        Task<UserProfile> UpdateByIdAsync(string userProfileId, UserProfile userProfile);
        Task DeleteByIdAsync(string userProfileId);
    }
}
