﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ToolKitGrid.Domain.HelperModels;

namespace ToolKitGrid.Repository
{
    public interface IUserSessionRepository
    {
        Task CreateAsync(UserSession userSession);
        Task<IEnumerable<UserSession>> GetAllAsync();
        Task<UserSession> GetByIdAsync(string sessionId);
        Task<UserSession> GetByUserIdAsync(string userId);
        Task<UserSession> UpdateByIdAsync(string id, UserSession session);
        Task DeleteByIdAsync(string id);
    }
}
