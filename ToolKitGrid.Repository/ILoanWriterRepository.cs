﻿using System.Threading.Tasks;
using ToolKitGrid.Domain.Models.Identity;

namespace ToolKitGrid.Repository
{
    public interface ILoanWriterRepository
    {
        Task CreateAsync(LoanWriter loanWriter);
        Task<LoanWriter> GetByUniqueIdAsync(string id);
        Task<LoanWriter> GetByEmailAsync(string email);
        Task<LoanWriter> UpdateByIdAsync(string uniqueId, LoanWriter loanWriter);
        Task DeleteByIdAsync(string uniqueId);
        Task<LoanWriter> GetUserByOneTimeUseTokenAsync(string oneTimeUseUserToken);
    }
}
