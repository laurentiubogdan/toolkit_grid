﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToolKitGrid.Domain.HelperModels;
using ToolKitGrid.Repository;

namespace ToolkitGrid.Repository.Mongo
{
    public class UserProfileRepository : IUserProfileRepository
    {
        private readonly MongoUnitOfWork _unitOfWork;

        public UserProfileRepository(MongoUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<UserProfile> CreateAsync(UserProfile userProfile)
        {
            await _unitOfWork.UserProfile.InsertOneAsync(userProfile);

            return userProfile;
        }

        public async Task<IEnumerable<UserProfile>> GetAllAsync()
        {
            return await _unitOfWork.UserProfile.Find(userProfile => true).ToListAsync();
        }

        public async Task<UserProfile> GetByIdAsync(string userProfileId)
        {
            return await _unitOfWork.UserProfile.Find(userProfile => userProfile.Id == userProfileId).FirstOrDefaultAsync();
        }

        public async Task<UserProfile> GetByUserIdAsync(string userId)
        {
            return await _unitOfWork.UserProfile.Find(userProfile => userProfile.UserId == userId).FirstOrDefaultAsync();
        }


        public async Task<UserProfile> UpdateByIdAsync(string userProfileId, UserProfile userProfile)
        {
            await _unitOfWork.UserProfile.ReplaceOneAsync(userProf => userProf.Id == userProfileId, userProfile);

            return await GetByIdAsync(userProfile.Id);
        }

        public async Task DeleteByIdAsync(string userProfileId)
        {
            await _unitOfWork.UserSession.DeleteOneAsync(userProfile => userProfile.Id == userProfileId);
        }
    }
}
