﻿"use strict";
define(["jquery"], function ($) {
    Object.defineProperty(Number.prototype, "ToViewNumber", {
        value: function ToViewNumber() {
            return this.toLocaleString(undefined, { minimumFractionDigits: 2 });
        },
        writable: false,
        configurable: false
    });

    function documentOnMouseUp(e) {
        var container = $("#user-profile-collapse");
        if (!container.is(e.target) && container.has(e.target).length === 0 && !$(e.target).hasClass("user-details-button")) {
            $("#user-profile-collapse").removeClass("show");
        }
    }
    $(document).on("mousedown", documentOnMouseUp);
});