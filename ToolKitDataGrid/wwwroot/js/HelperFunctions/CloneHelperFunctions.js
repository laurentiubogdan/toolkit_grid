﻿define(["jquery"], function ($) {

    function CloneHelperFunctions() { }

    CloneHelperFunctions.prototype.createListForExpenses = function (expenses) {
        var self = this;
        var expensesResult = [];
        if (expenses !== null) {
            expenses.forEach(function (item) {
                var resultAmount = 0;
                for (var property in item) {
                    if (property.indexOf("Amount") >= 0) {
                        resultAmount = resultAmount + parseFloat(item[property]);
                    }
                }
                if (resultAmount !== null && isNaN(resultAmount) === false) {
                    var result = "Expenses - $" + self.getFormatValue(resultAmount) + " (Monthly)";
                    expensesResult.push(result);
                }
            });
        }

        return expensesResult;
    };

    CloneHelperFunctions.prototype.createListForLoan = function (loans) {
        var self = this;
        var loansResult = [];
        if (loans !== null) {
            loans.forEach(function (item) {
                var result = item.ProductName + " - $" + self.getFormatValue(item.AmountRequested);
                loansResult.push(result);
            });
        }

        return loansResult;
    };

    CloneHelperFunctions.prototype.createListForLiability = function (liability) {
        var self = this;
        var liabilityResult = [];
        if (liability !== null) {
            liability.forEach(function (item) {
                if (item.FinancialInstitution === "ANZ Bank") {
                    var result = item.Type + " - $" + self.getFormatValue(item.OutstandingBalance) + "(ANZ Bank)";
                } else {
                    if (item.OtherFIName !== undefined) {
                        result = item.Type + " - $" + self.getFormatValue(item.OutstandingBalance) + "(" + item.OtherFIName + ")";
                    } else {
                        result = item.Type + " - $" + self.getFormatValue(item.OutstandingBalance);
                    }
                }
                liabilityResult.push(result);
            });
        }
        return liabilityResult;
    };

    CloneHelperFunctions.prototype.createListForIncome = function (income, applicants) {
        var self = this;
        var incomeResult = [];
        if (income !== null) {
            income.forEach(function (item) {
                var result = null;
                if (item !== null) {
                    if (item.Owner !== undefined) {

                        if ($.isArray(item.Owner)) {
                            var owners = [...item.Owner];
                            for (var i = 0; i < owners.length; i++) {
                                result = item.Type + " " + "(" + self.getApplicantById(applicants, owners[i].x_Party) + ")";
                                incomeResult.push(result);
                            }
                        } else {
                            result = item.Type + " " + "(" + self.getApplicantById(applicants, item.Owner.x_Party) + ")";
                            incomeResult.push(result);
                        }
                    }
                }
            });
        }

        return incomeResult;
    };

    CloneHelperFunctions.prototype.getApplicantById = function (applicants, id) {
        var result = null;
        if (applicants !== null) {
            applicants.forEach(function (item) {

                if (item.UniqueID === id) {
                    result = item.FirstName + " " + item.Surname;
                }
            });
        }

        return result;
    };

    CloneHelperFunctions.prototype.createListForApplicants = function (applicants) {
        var applicantsResult = [];
        if (applicants !== null) {
            applicants.forEach(function (item) {
                if (item.FirstName !== null || item.Surname !== null) {
                    if (item.PrimaryApplicant === "Yes") {
                        var result = item.FirstName + " " + item.Surname + " " + "(Primary Applicant)";
                    } else {
                        result = item.FirstName + " " + item.Surname;
                    }
                    applicantsResult.push(result);
                }
            });
        }

        return applicantsResult;
    };

    CloneHelperFunctions.prototype.createListForHousehold = function (array, type) {
        var arrayResult = [];

        if (array !== null) {
            array.forEach(function (item) {
                arrayResult.push(item[type]);
            });
        }

        return arrayResult;
    };

    CloneHelperFunctions.prototype.getFormatValue = function (num, decimals) {
        if (num !== null && num !== undefined) {
            var str = num.toString().replace("", ""),
                parts = false,
                output = [],
                i = 1,
                formatted = null;
            if (str.indexOf(".") > 0) {
                parts = str.split(".");
                str = parts[0];
            }
            str = str.split("").reverse();
            for (var j = 0, len = str.length; j < len; j++) {
                if (str[j] !== ",") {
                    output.push(str[j]);
                    if (i % 3 === 0 && j < (len - 1)) {
                        output.push(",");
                    }
                    i++;
                }
            }
            formatted = output.reverse().join("");
            return ("" + formatted + ((parts) ? "." + parts[1].substr(0, decimals) : ""));
        }
    };

    return CloneHelperFunctions;
});