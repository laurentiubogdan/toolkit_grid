﻿define([], function () {
    function GeneralHelperFunctions() { }

    GeneralHelperFunctions.prototype.getRelativeTimeDifference = function (current, previous) {
        previous = new Date(Date.parse(previous));
        var msPerMinute = 60 * 1000;
        var msPerHour = msPerMinute * 60;
        var msPerDay = msPerHour * 24;
        var msPerMonth = msPerDay * 30;
        var msPerYear = msPerDay * 365;

        var elapsed = current - previous;

        if (elapsed < msPerMinute) {
            return Math.round(elapsed / 1000) + ' seconds ago';
        } else if (elapsed < msPerHour) {
            return Math.round(elapsed / msPerMinute) + ' minutes ago';
        } else if (elapsed < msPerDay) {
            return Math.round(elapsed / msPerHour) + ' hours ago';
        } else if (elapsed < msPerMonth) {
            return Math.round(elapsed / msPerDay) + ' days ago';
        } else if (elapsed < msPerYear) {
            return Math.round(elapsed / msPerMonth) + ' months ago';
        } else {
            return Math.round(elapsed / msPerYear) + ' years ago';
        }
    };

    GeneralHelperFunctions.prototype.getScrollBarWidth = function () {
        var inner = document.createElement('p');
        inner.style.width = "100%";
        inner.style.height = "200px";

        var outer = document.createElement('div');
        outer.style.position = "absolute";
        outer.style.top = "0px";
        outer.style.left = "0px";
        outer.style.visibility = "hidden";
        outer.style.width = "200px";
        outer.style.height = "150px";
        outer.style.overflow = "hidden";
        outer.appendChild(inner);

        document.body.appendChild(outer);
        var w1 = inner.offsetWidth;
        outer.style.overflow = 'scroll';
        var w2 = inner.offsetWidth;
        if (w1 === w2) w2 = outer.clientWidth;

        document.body.removeChild(outer);

        return w1 - w2;
    };

    GeneralHelperFunctions.prototype.formateDate = function (date) {
        date = new Date(Date.parse(date));
        return ("0" + date.getUTCDate()).slice(-2) + "/" +
            ("0" + (date.getUTCMonth() + 1)).slice(-2) + "/" +
            date.getUTCFullYear() + " " +
            ("0" + date.getUTCHours()).slice(-2) + ":" +
            ("0" + date.getUTCMinutes()).slice(-2) + ":" +
            ("0" + date.getUTCSeconds()).slice(-2);
    };

    GeneralHelperFunctions.prototype.parseArray = function (arrayData) {
        var array = [];
        if (arrayData[0] !== null) {
            array = [...arrayData];
            return array;
        }
        return array;
    };

    return GeneralHelperFunctions;
});