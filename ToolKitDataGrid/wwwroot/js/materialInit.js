﻿"use strict";
define(["jquery", "material"],
    function ($, mdc) {

        const textField = [].map.call($('.mdc-text-field'), function (el) {
            mdc.textField.MDCTextField.attachTo(el);
        });
        const select = [].map.call($('.mdc-select'), function (el) {
            mdc.select.MDCSelect.attachTo(el);
        });
        const ripple = [].map.call($(".mdc-line-ripple"), function (el) {
            mdc.ripple.MDCRipple.attachTo(el);
        });
        const ripple2 = [].map.call($(".mdc-button"), function (el) {
            mdc.ripple.MDCRipple.attachTo(el);
        });
        const floating = [].map.call($(".mdc-floating-label"), function (el) {
            mdc.floatingLabel.MDCFloatingLabel.attachTo(el);
        });
        const icon = [].map.call($(".mdc-text-field-icon"), function (el) {
            mdc.textFieldIcon.MDCTextFieldIcon.attachTo(el);
        });
        const menu = [].map.call($(".mdc-menu"), function (el) {
            return mdc.menu.MDCMenu.attachTo(el);
        });

        //$(".design-button").on("click", function () {
        //    if ($(".mdc-menu-surface").hasClass("mdc-menu-surface--open")) {
        //        menu[0].open = false;
        //    } else {
        //        menu[0].open = true;
        //    }
        //});
    });