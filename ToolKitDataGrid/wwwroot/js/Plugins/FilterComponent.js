﻿define(["jquery", "multiselect"], function ($) {
    function FilterComponent(configurations, callback) {
        var self = this;

        FilterComponent.defaultConfigurations = {
            $select: $("#filter-select"),
            multiselectOptions: {
                includeSelectAllOption: true,
                nonSelectedText: 'Choose to filter',
                numberDisplayed: 2,
                buttonText: function (options, select) {
                    var numberOfOptions = $(select).children('option').length;

                    if (options.length === 0) {
                        return this.nonSelectedText;
                    }
                    else if (options.length === numberOfOptions) {
                        return this.allSelectedText;
                    }
                    else if (options.length <= this.numberDisplayed) {
                        var values = [];

                        $.each(options, function () {
                            if (self.$select.attr("id") == "grid-column-multiselect") {
                                var option = this.value.replace(/([A-Z])/g, ' $1').trim();
                            } else {
                                var option = this.value
                            }

                            values.push(option);
                        });

                        return `${this.nSelectedText} ${values.join(', ')}`;
                    } else if (options.length > this.numberDisplayed) {
                        return `${this.nSelectedText} ${options.length} selected`;
                    }
                },
                buttonClass: "btn button default-button",
                onChange: function () {
                    self.FilterChange();
                },
                onSelectAll: function () {
                    self.FilterChange();
                },
                onDeselectAll: function () {
                    self.FilterChange();
                }
            },
            data: [{}]
        };

        self.configurations = $.extend(true, {}, FilterComponent.defaultConfigurations, configurations);
        self.callback = callback;

        // options variables
        self.$select = self.configurations.$select;
        self.multiselectOptions = self.configurations.multiselectOptions;
        self.data = self.configurations.data;
        self.selectId = self.$select.attr("id");
    }

    FilterComponent.prototype.Activate = function () {
        var self = this;

        self.$select.multiselect(self.multiselectOptions);
        self.$select.multiselect("dataprovider", self.data);

        if (self.$select.hasClass("bootstrap-multiselect")) {
            $.each($(`#${self.selectId} + div.btn-group li a label input`), function () {
                var $input = $(this);

                $input.addClass("mdc-checkbox__native-control");
                $input.after($("#mdc-checkbox-template").html());
            });
        }
    };

    FilterComponent.prototype.FilterChange = function () {
        var self = this,
            selectedOptions = self.$select.val();

        var filter = {
            logic: "or",
            filters: []
        };

        $.each(selectedOptions, function () {
            // "this" is a primitive value of String that is returned with valueOf()
            var option = this.valueOf();

            filter.filters.push({ field: "Status", operator: "contains", value: option });
        });

        self.callback(filter);
    };

    return FilterComponent;
});