﻿requirejs.config({
    baseUrl: '/',
    "waitSeconds": 20,
    paths: {
        "jquery": "vendor/jquery/jquery.min",
        "bootstrap": "vendor/bootstrap/bootstrap.bundle.min",
        "material": "vendor/material-components-web/material-components-web.min",
        "knockout": "vendor/knockout/knockout-latest",
        "slideModal": "vendor/SlideModal/SlideModal",
        "expressionsEngine": "vendor/ExpressionEngine/ExpressionsEngine",
        "multiselect": "lib/bootstrap-multiselect/dist/js/bootstrap-multiselect",
        "formatNumber": "vendor/jquery-number/jquery.number"
    },
    urlArgs: 'v=6.4.0.0',
    optimize: "uglify",
    config: {
        text: {
            useXhr: function () {
                // allow cross-domain requests
                // remote server allows CORS
                return false;
            }
        }
    },
    packages: [
        //{
        //    name: "jsonValidation",
        //    location: "node_modules/JSONFormsValidation",
        //    main: "JsonFormsValidation"
        //},
        {
            name: "jsonForm",
            location: "vendor/JsonForms",
            main: "JsonForms"
        }
    ]
});

// Insert here in case the appending process doesnt work like in kendo.
(function () {
    var load = requirejs.load;
    requirejs.load = function (context, moduleId, url) {
        if (moduleId && moduleId.startsWith("kendo.")) {
            // if moduleId starts with "kendo.", we overwrite the url path to the kendo library location
            url = url.substring(url.lastIndexOf("/") + 1);
            url = "/vendor/KendoUI/js/" + url;
        }
        load.apply(this, arguments);
    };
}());