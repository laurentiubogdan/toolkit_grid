﻿define(["jquery", "slideModal", "jsonForm"],
    function ($, SlideModal, JsonForm) {
        $("#loader-wrapper").fadeOut();

        var userData = {};
        var schema = {};

        //GET USERDATA FROM SERVER SIDE AJAX
        function getData() {
            return $.ajax({
                url: "/Account/GetUserProfile",
                type: "GET",
                complete: function (data, status) {
                    if (status === "success") {
                        userData = data.responseJSON;
                    }
                    else {
                        console.log("Error on getting JSON from server: ", data);
                    }
                }
            });
        }

        //GET SCHEMA JSON FORM
        function getSchema() {
            var serviceUrl = "/ServiceJsonFiles/UserDataForm_v2.json";
            return $.ajax({
                url: serviceUrl,
                type: "GET",
                complete: function (data, status) {
                    if (status === "success") {
                        schema = data.responseJSON;
                    }
                    else {
                        console.log("Error on getting JSON from server: ", data);
                    }
                }
            });
        }

        
        var myData = {};

        //iniate slide modal + json forms
        getData().then(getSchema()).then(function () {
            var controls = schema.sections[0].form.controls;

            //prefix lockedFields
            var lockedFields = [];


            for (var i = 0, len = userData.LockedFields.length; i < len; i++) {
                var originalLockedField = userData.LockedFields[i];
                lockedFields.push("record.User." + originalLockedField);
            }

            //set default value
            myData = userData.ExternalLoanWriterModel; 

            var modalOptions = {
                $container: $(".plugin-container"),
                $modalContainer: $("#controlsContainer"),
                template: "slideModalTemplate",
                modalId: "modalId",
                title: "UPDATE PROFILE",
                mainButtonText: "Update",
                mainButtonHandler: function () {
                    $("#loader-wrapper").show();
                    $.ajax({
                        url: "/account/updateProfile",
                        type: "POST",
                        data: myData,
                        complete: function (data, status) {
                            if (status === "success") {
                                var responseJson = data.responseJSON;
                                if (responseJson.success === true) {
                                    window.location.href = "/";
                                }
                            }
                            else {
                                console.log("Error on getting JSON from server: ", data);
                            }
                        }
                    });
                },
                onSetContent: function () {
                    this.jsonForms = new JsonForm({
                        $container: $("#controlsContainer"),
                        name: "User",
                        controls: controls,
                        uniqueId: userData.ExternalLoanWriterModel.UniqueID,
                        onSaveResultData: function (resultData) {
                            myData = resultData;
                        },
                        record: {
                            User: [
                                userData.ExternalLoanWriterModel
                            ]
                        },
                        lockedFields: lockedFields
                    });
                },
                onDestroyContent: function () {
                    //this.jsonForms.destroy();
                },
                displaySecondaryButton: false,
                position: SlideModal.Position.Center,
                lightbox: SlideModal.Lightbox.Black
            };

            window.slideModal = new SlideModal(modalOptions);
            slideModal.showModal();
        });

    });