﻿"use strict";
define(["jquery", "knockout", "expressionsEngine", "js/Plugins/FilterComponent.js", "slideModal", "material", "jsonForm", "js/HelperFunctions/GeneralHelperFunctions", "js/HelperFunctions/CloneHelperFunctions", "js/Plugins/JsonStructureS3Handler", "kendo.grid.min", "kendo.aspnetmvc.min", "bootstrap"],
    function ($, ko, ExpressionEngine, FilterComponent, SlideModal, mdc, JsonForm, GeneralHelperFunctions, CloneHelperFunctions, JsonStructureS3Handler) {
        //functions that need to be known before the kendo grid is rendered
        var cloneHelperFunctions = new CloneHelperFunctions,
            generalHelperFunctions = new GeneralHelperFunctions,
            today = new Date(),
            myData = {},
            $loader = $("#loader-wrapper"),
            record = 0,
            createRecordJson = {},
            jsonStructureS3Handler = new JsonStructureS3Handler({
                keyName: "Qualify-Resources-JSON-Forms/USER/UserRolePermissionsStructureV",
                localStorageKey: "UserRolePermissionsStructureV",
                jsonVersionsStructure: "UserRolePermissionsStructure"
            });

        async function getRolesForUser() {
            var columns = [],
                columnsForRemove = [],
                testNowRoles = ["Admin"],
                columnsUse = {};

            await new Promise((resolve, reject) => {
                jsonStructureS3Handler.GetJsonVersions(function (structuralJson) {
                    structuralJson.gridColumns.forEach(function (item) {
                        if (item.roles.some(r => testNowRoles.includes(r))) {
                            var column = {
                                label: item.value,
                                value: item.value.replace(/\s/g, ''),
                                selected: true
                            };
                            if (column.value === "RecordName") {
                                column.disabled = true;
                            }
                            columns.push(column);
                        } else {
                            columnsForRemove.push(item.value);
                        }
                    });

                    columnsUse.columnsForRemove = columnsForRemove;
                    columnsUse.columns = columns;
                    resolve();
                });
            });

            return Promise.resolve(columnsUse);
        }

        getRolesForUser()
            .then(function (columnsUse) {
                var columnsKendo = [{
                    title: "#",
                    customIdentifier: "RecordNumber",
                    template: function () {
                        return ++record;
                    },
                    width: "5%"
                },
                {
                    field: "Id",
                    title: "Id",
                    hidden: true
                },
                {
                    field: "UniqueId",
                    title: "UniqueId",
                    hidden: true
                },
                {
                    field: "RecordName",
                    title: "Record Name",
                    hidden: true,
                    template: function (dataItem) {
                        return "<span class='record-name'>" + dataItem.RecordName + "</span>";
                    }
                },
                {
                    field: "Applicants",
                    title: "Applicants",
                    hidden: true,
                    width: "20%",
                    template: function (dataItem) {
                        if (dataItem.Applicants.length === 0) {
                            return "-";
                        }
                        return dataItem.Applicants;
                    }
                },
                {
                    field: "CreatedBy",
                    title: "Created By",
                    hidden: true,
                    width: "15%",
                    template: function (dataItem) {
                        if (!dataItem.CreatedBy) {
                            return "-";
                        } else {
                            return dataItem.CreatedBy;
                        }
                    }
                },
                {
                    field: "DateCreated",
                    title: "Date Created",
                    hidden: true,
                    width: "12.5%",
                    template: function (dataItem) {
                        return `<span data-toggle="tooltip" data-placement="top" title=" ` + generalHelperFunctions.formateDate(dataItem.DateCreated) + `">` + generalHelperFunctions.getRelativeTimeDifference(today, dataItem.DateCreated) + `</span>`;
                    }
                },
                {
                    field: "UpdatedBy",
                    title: "Updated By",
                    hidden: true,
                    width: "15%",
                    template: function (dataItem) {
                        if (!dataItem.UpdatedBy) {
                            return "-";
                        } else {
                            return dataItem.UpdatedBy;
                        }
                    }
                },
                {
                    field: "DateUpdated",
                    title: "Date Updated",
                    hidden: true,
                    width: "12.5%",
                    template: function (dataItem) {
                        if (!dataItem.DateUpdated || dataItem.DateUpdated === "0001-01-01T00:00:00Z") {
                            return "-";
                        } else {
                            return `<span data-toggle="tooltip" data-placement="top" title=" ` + generalHelperFunctions.formateDate(dataItem.DateUpdated) + `">` + generalHelperFunctions.getRelativeTimeDifference(today, dataItem.DateUpdated) + `</span>`;
                        }
                    }
                },
                {
                    customIdentifier: "RecordOptions",
                    template: function () {
                        return `<div class="dropdown grid-dropdown">
                            <button tabindex="0" class="btn more-info-button-grid dropdown-toggle" type="button" data-toggle="dropdown" id="dropdownMenuButtonGrid-` + record + `" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" id="dropdownMenu-` + record + `" style="z-index: 1;" aria-labelledby="dropdownMenuButtonGrid-` + record + `">
                                <a class="dropdown-item archive" href="#">Archive</a>
                                <a class="dropdown-item clone" href="#">Clone</a>
                                <a class="dropdown-item delete" href="#">Delete</a>
                            </div>
                         </div>`;
                    },
                    width: "2.5%",
                    attributes: {
                        style: "overflow: inherit"
                    }
                },
                {
                    customIdentifier: "Checkboxes",
                    selectable: true,
                    width: "2.5%"
                }
                ];

                columnsKendo = columnsKendo.filter(function (item) {
                    return columnsUse.columnsForRemove.indexOf(item.field) === -1;
                });

                $("#grid").kendoGrid({
                    autoBind: false,
                    dataSource: {
                        type: "aspnetmvc-ajax",
                        transport: {
                            read: "/Record/GridListRecords"
                        },
                        schema: {
                            data: function (response) {
                                return response.GridRecords;
                            }
                        },
                        serverFiltering: true,
                        serverSorting: true,
                        serverPaging: true,
                        requestEnd: function (e) {
                            this.total = function () {
                                return e.response.RecordsCount;
                            };
                            $loader.fadeOut();
                        }
                    },
                    pageable: {
                        pageSizes: true,
                        buttonCount: 5
                    },
                    sortable: true,
                    scrollable: false,
                    columns: columnsKendo
                });

                return Promise.resolve(columnsUse);

            })
            .then(function (columnsUse) {
                var typingTimer = null,
                    $grid = $("#grid").data("kendoGrid"),
                    $searchTextBox = $("#search-input"),
                    sessionId = localStorage.getItem("SessionId"),
                    $clear = $(".clear"),
                    initialPageSize = null,
                    userProfile = null,
                    multiselectFilters = [],
                    statusMultiselectFilter = {},
                    userMultiselectFilter = {},
                    $kGrid = $(".k-grid"),
                    $gridContent = $(".k-grid tbody"),
                    $mainContainer = $(".main-container"),
                    searchMade = false,
                    jsonStructureS3Handler = new JsonStructureS3Handler({
                        keyName: "Qualify-Resources-JSON-Forms/CREATE-RECORD/CreateRecordStructureV",
                        localStorageKey: "CreateRecordStructureV",
                        jsonVersionsStructure: "CreateRecordStructure"
                    });

                getUserProfileData();

                jsonStructureS3Handler.GetJsonVersions(function (structuralJson) {
                    createRecordJson = structuralJson.controls;
                });

                var $columnMultiple = $(`<select id="grid-column-multiselect" class="bootstrap-multiselect" multiple></select>`);
                $(".k-pager-wrap").append($columnMultiple);

                var culumnsFilter = new FilterComponent({
                    $select: $columnMultiple,
                    multiselectOptions: {
                        nonSelectedText: 'Columns: None',
                        allSelectedText: 'Columns: All',
                        nSelectedText: 'Columns:',
                        onChange: function () {
                            displayColumn();
                            userProfile.VisibleColumns = getColumnMultiselectValue();
                            updateUserProfileSettings(userProfile);
                        },
                        onSelectAll: function () {
                            displayColumn();
                            userProfile.VisibleColumns = getColumnMultiselectValue();
                            updateUserProfileSettings(userProfile);
                        },
                        onDeselectAll: function () {
                            displayColumn();
                            userProfile.VisibleColumns = getColumnMultiselectValue();
                            updateUserProfileSettings(userProfile);
                        }
                    },
                    data: columnsUse.columns
                }, function (filter) {
                    if (filter.filters.length > 0) {
                        statusMultiselectFilter = filter;
                    } else {
                        statusMultiselectFilter = {};
                    }
                    gridSearch();
                });
                culumnsFilter.Activate();

                var statusFilter = new FilterComponent({
                    $select: $("#status-multiselect"),
                    multiselectOptions: {
                        nonSelectedText: 'Status: None',
                        allSelectedText: 'Status: All',
                        nSelectedText: 'Status:'
                    },
                    data: [
                        { label: 'UMI Result', value: 'UMI Result' },
                        { label: 'Pricing Request Sent', value: 'Pricing Sent' },
                        { label: 'Pricing Result', value: 'Pricing Result' },
                        { label: 'Archived', value: 'Archived' },
                        { label: 'New', value: 'New' }
                    ]
                }, function (filter) {
                    if (filter.filters.length > 0) {
                        statusMultiselectFilter = filter;
                    } else {
                        statusMultiselectFilter = {};
                    }
                    gridSearch();
                });
                statusFilter.Activate();

                // Event listeners
                (function () {
                    $(window).resize(function () {
                        $grid.resize();
                    });

                    $searchTextBox.on('keyup', function (e) {
                        if ($searchTextBox.val()) {
                            $clear.show();
                        } else {
                            $clear.hide();
                        }
                        if ($searchTextBox.val().length > 1) {
                            clearTimeout(typingTimer);
                            typingTimer = setTimeout(doneTyping, 300);
                        } else if ($searchTextBox.val().length === 0) {
                            clearTimeout(typingTimer);
                            if (searchMade === true) {
                                doneTyping();
                                searchMade = false;
                            }
                        }
                        if (e.keyCode === 13 && $searchTextBox.val().length > 0) {
                            clearTimeout(typingTimer);
                            doneTyping();
                        }
                    });

                    $searchTextBox.on('keydown', function () {
                        clearTimeout(typingTimer);
                    });

                    $clear.on("click", function () {
                        clearTimeout(typingTimer);
                        $searchTextBox.val("");
                        if (searchMade === true) {
                            doneTyping();
                        }
                        searchMade = false;
                        $clear.hide();
                    });

                    // Kendo listeners
                    $kGrid.on("click", "tbody tr", function (e) {
                        // if checkbox was clicked
                        if ($(e.target).hasClass("k-checkbox")
                            || $(e.target).hasClass("k-checkbox-label")
                            || $(e.target).hasClass("more-info-button-grid")
                            || $(e.target).hasClass("fa-ellipsis-v")) {
                            return;
                        }

                        clearGridSelection();

                        var $row = $(this);
                        selectItem($row);
                    });

                    $kGrid.on("show.bs.dropdown", ".dropdown", function () {
                        clearGridSelection();

                        var $row = $(this).closest("tr");
                        selectItem($row);
                    });

                    $mainContainer.on("mouseleave", ".dropdown-menu.show", function () {
                        if ($(".dropdown-toggle[aria-expanded='true']").is(":hover")) {
                            return;
                        }

                        $(this).parent().dropdown('toggle');
                    });

                    $mainContainer.on("mouseleave", ".dropdown-toggle[aria-expanded='true']", function () {
                        var hoveredToElement = $(":hover").last();
                        var $dropdownMenu = $(this).next();

                        if ($dropdownMenu.is(hoveredToElement) || $dropdownMenu.has(hoveredToElement).length > 0) {
                            return;
                        }

                        $(this).dropdown('toggle');
                    });

                    $("body").on("click", " .mdc-list-item--selected", function () {
                        if ($(this).textContent === "") {
                            $(this).removeClass("mdc-list-item--selected");
                        }
                    });

                    $grid.bind("dataBound", function () {
                        var pageSize = $grid.dataSource.pageSize();

                        if (initialPageSize !== null && initialPageSize !== pageSize) {
                            userProfile.ItemsPerPage = pageSize;
                            updateUserProfileSettings(userProfile);
                        }

                        setTimeout(function (e) {
                            $(".k-loading-image").show();
                        });
                    });

                    $grid.bind("dataBinding", function () {
                        var page = this.dataSource.page();
                        var pageSize = this.dataSource.pageSize();
                        if (!pageSize) {
                            pageSize = 20;
                        }
                        record = (page - 1) * pageSize;
                    });

                    $grid.bind("change", function () {
                        //removing custom selection
                        deselectAllItems();

                        var selected = $.map(this.select(), function (item) {
                            selectItem($(item));
                            return $(item);
                        });

                        if (selected.length > 1) {
                            $(".k-design-button").prop('disabled', true);
                        } else {
                            $(".k-design-button").prop('disabled', false);
                        }

                        toogleMainDropdown();
                    });

                    $(".create-button").on("click", function () {
                        _createModalForCreate();
                    });

                    $(".main-clone").on("click", function () {
                        var selectedItem = getGridSelectedRowsData($grid, true)[0];
                        cloneItem(selectedItem);
                    });

                    $(".main-delete").on("click", function () {
                        var selectedRows = getGridSelectedRowsData($grid, true),
                            deleteOptions = initOptionsDeleteRecord(selectedRows);

                        deleteItems(deleteOptions);
                    });

                    $(".main-archive").on("click", function () {
                        var selectedRows = getGridSelectedRowsData($grid, true),
                            archiveOptions = initOptionsArchiveRecord(selectedRows);

                        archiveItems(archiveOptions);
                    });

                    $gridContent.on("click", ".clone", function () {
                        var selectedItem = getGridSelectedRowsData($grid)[0];

                        cloneItem(selectedItem);
                    });

                    $gridContent.on("click", ".delete", function () {
                        var selectedRows = getGridSelectedRowsData($grid),
                            deleteOptions = initOptionsDeleteRecord(selectedRows);

                        deleteItems(deleteOptions);
                    });

                    $gridContent.on("click", ".archive", function () {
                        var selectedRows = getGridSelectedRowsData($grid),
                            archiveOptions = initOptionsArchiveRecord(selectedRows);

                        archiveItems(archiveOptions);
                    });

                    $("body").on("click", ".record-name", function () {
                        var rowDataItem = $grid.dataItem(this.closest("tr"));
                        $loader.show();
                        $.ajax({
                            url: "/Record/SendRecordToQualify",
                            type: "Post",
                            data: {
                                uniqueId: rowDataItem.UniqueId
                            },
                            success: function (response) {
                                window.open(response.Url, "_self");
                            }
                        });
                    });

                    $("body").on("mousedown", function (e) {
                        var $target = $(e.target),
                            $modalDialog = $(".modal-dialog");

                        if (!$modalDialog.is(e.target)
                            && $modalDialog.has(e.target).length === 0
                            && !$(e.target).hasClass("k-item")
                            && !$(e.target).hasClass("k-calendar-container")
                            && !$(e.target).hasClass("k-icon")
                            && !$(e.target).hasClass("k-header")
                            && !$(e.target).hasClass("k-content")
                            && !$(e.target).hasClass("k-month")
                            && !$(e.target).hasClass("k-calendar-view")
                            && !$(e.target).hasClass("k-list-container")
                            && !$(e.target).hasClass("k-list-scroller")
                            && !$(e.target).hasClass("k-nodata")
                            && !$(e.target).hasClass("k-link")) {

                            if (e.target.title !== undefined) {
                                var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                                var isDay = false;
                                var day = e.target.title;

                                for (var i = 0; i < days.length; i++) {
                                    if (day === days[i]) {
                                        isDay = true;
                                        break;
                                    }
                                }
                                if (isDay === false) {
                                    $(".plugin-container").children().remove();
                                    $("body").removeClass("modal-open");
                                }
                            }
                        }

                        if ($gridContent.has($target).length > 0 || $modalDialog.has($target).length > 0) {
                            return;
                        }

                        //removing custom selection
                        deselectAllItems();
                    });
                })();

                function getGridSelectedRowsData(grid, fromCheckbox) {
                    var rows = [],
                        $selectedRows = null;

                    if (fromCheckbox) {
                        $selectedRows = grid.select();
                    } else {
                        $selectedRows = $("#grid table tbody .kendo-row-selected");
                    }

                    $.each($selectedRows, function () {
                        var row = this;
                        rows.push(grid.dataItem(row));
                    });
                    return rows;
                }

                function toogleMainDropdown() {
                    var selectedRows = getGridSelectedRowsData($grid);
                    if (selectedRows.length > 0) {
                        $(".main-archive").removeClass("disabled");
                        $(".main-delete").removeClass("disabled");
                        if (selectedRows.length === 1) {
                            $(".main-clone").removeClass("disabled");
                        } else {
                            $(".main-clone").addClass("disabled");
                        }
                    } else {
                        $(".main-archive").addClass("disabled");
                        $(".main-clone").addClass("disabled");
                        $(".main-delete").addClass("disabled");
                    }
                }

                function clearGridSelection() {
                    //removing custom selection
                    deselectAllItems();

                    // removing kendo buildIn selection
                    $grid.clearSelection();
                }

                function selectItem($row, withCheckbox) {
                    $row.addClass("kendo-row-selected");

                    if (withCheckbox) { }
                }

                function deselectItem($row, withCheckbox) {
                    $row.removeClass("kendo-row-selected");

                    if (withCheckbox) { }
                }

                function deselectAllItems() {
                    var $selectedRows = $("#grid tbody tr.kendo-row-selected");

                    $.each($selectedRows, function () {
                        deselectItem($(this));
                    });
                }

                function deleteItems(selectedItems) {
                    var modalMessage = {
                        mainMessage: "You are about to DELETE the following records: ",
                        question: "This action cannot be undone. Are you sure you want to delete the records?",
                        mainButtonText: "Delete"
                    };

                    _createModalForMessages(selectedItems, modalMessage);
                }

                function archiveItems(selectedItems) {
                    var modalMessage = {
                        mainMessage: "You are about to ARCHIVE the following records: ",
                        question: "This action cannot be undone. Are you sure you want to archive the records?",
                        mainButtonText: "Archive"
                    };
                    _createModalForMessages(selectedItems, modalMessage);
                }

                function cloneItem(selectedItem) {
                    $.ajax({
                        url: "/Record/GetRecordByUniqueIdAsync",
                        type: "GET",
                        data: {
                            uniqueId: selectedItem.UniqueId
                        },
                        success: function (response) {
                            var optionsCloneRecord = {
                                modalId: "cloneRecordModal",
                                modalTitle: "CLONE RECORD",
                                mainButtonText: "Clone Record",
                                mainButtonHandler: function () {
                                    $.ajax({
                                        url: "/Record/CloneAsync",
                                        type: "Post",
                                        data: {
                                            RecordName: $("#inputCloneRecord").val(),
                                            UniqueID: response.UniqueID
                                        },
                                        complete: function (response) {
                                            $(".plugin-container").children().remove();
                                            $("body").removeClass("modal-open");
                                            $(".mdc-select__menu ").remove();
                                            $grid.dataSource.read();
                                        }
                                    });
                                },
                                cancelButtonHandler: function () {
                                },
                                controls: []
                            };
                            _createModalForClone(optionsCloneRecord, response);
                        }
                    });
                }

                function doneTyping() {
                    gridSearch(multiselectFilters);
                    searchMade = true;
                }

                function displayColumn() {
                    var columns = $grid.columns;
                    var displayColumnArray = $columnMultiple.val();
                    var columnsToCheck = [];

                    displayColumnArray.push("RecordName");

                    $.each(columns, function () {
                        var column = this;
                        if (column.title !== "#" && column.field !== "Id" && column.field !== undefined) {
                            columnsToCheck.push(column);
                        }
                    });

                    $.each(columnsToCheck, function () {
                        var column = this;
                        if (displayColumnArray.includes(column.field)) {
                            $grid.showColumn(column.field);
                        } else {
                            $grid.hideColumn(column.field);
                        }
                    });
                }

                function setUserProfileSettings(userProfile) {
                    $grid.dataSource.pageSize(userProfile.ItemsPerPage);
                    initialPageSize = $grid.dataSource.pageSize();

                    var gridOptions = $.extend({}, $grid.getOptions());

                    if (gridOptions.columns[0].width.replace(/[^0-9]/g, '') / 100 * screen.width < 50) {
                        gridOptions.columns.forEach(function (item) {
                            switch (item.field) {
                                case "Applicants":
                                    item.width = 200;
                                    break;
                                case "CreatedBy":
                                    item.width = 150;
                                    break;
                                case "DateCreated":
                                    item.width = 100;
                                    break;
                                case "UpdatedBy":
                                    item.width = 150;
                                    break;
                                case "DateUpdated":
                                    item.width = 100;
                                    break;
                            }
                            if (item.hasOwnProperty("customIdentifier")) {
                                item.width = 50;
                            }
                        });
                    }

                    $grid.columns = gridOptions.columns;
                    $('#grid-column-multiselect').multiselect('deselectAll', false);
                    $('#grid-column-multiselect').multiselect('updateButtonText');
                    $("#grid-column-multiselect").multiselect("select", userProfile.VisibleColumns);
                    displayColumn();
                }

                function updateUserProfileSettings(userProfile) {
                    $.ajax({
                        url: "/UserProfile/UpdateUserProfile",
                        type: "Post",
                        data: {
                            profile: userProfile
                        },
                        complete: function (response) { }
                    });
                }
                //Set the filter 
                function gridSearch() {

                    var searchValue = $.trim($searchTextBox.val()),
                        gridFilter = [],
                        baseFilter = getGridBaseFilter(searchValue),
                        modifiedFilter = [];

                    if (searchValue !== "") {
                        $.each(baseFilter, function () {
                            var filter = this;

                            filter["value"] = searchValue;
                            modifiedFilter.push(filter);
                        });

                        var normalFilter = {
                            logic: "or",
                            filters: modifiedFilter
                        };

                        gridFilter.push(normalFilter);
                    }

                    if (Object.keys(statusMultiselectFilter).length > 0) {
                        gridFilter.push(statusMultiselectFilter);
                    } else if (Object.keys(userMultiselectFilter).length > 0) {
                        gridFilter.push(userMultiselectFilter);
                    }

                    $grid.dataSource.filter(gridFilter);

                    multiselectFilters = [];

                    function getGridBaseFilter(searchValue) {
                        var baseFilter = [
                            { field: "RecordName", operator: "contains" },
                            { field: "Person", operator: "contains" },
                            { field: "CreatedBy", operator: "contains" },
                            { field: "UpdatedBy", operator: "contains" }
                        ];

                        if (searchValue.includes("/") || searchValue.includes("-") || searchValue.includes(".")) {
                            baseFilter.push({ field: "DateCreated", operator: "contains" });
                            baseFilter.push({ field: "DateUpdate", operator: "contains" });
                        }

                        return baseFilter;
                    }
                }

                function getColumnMultiselectValue() {
                    var value = $('#grid-column-multiselect').val();
                    value.push("RecordName");

                    return value;
                }

                function getUserProfileData() {
                    $.ajax({
                        url: "/Record/GetUserSession",
                        type: "Post",
                        data: {
                            sessionId: sessionId
                        },
                        complete: function (response) {
                            var userId = response.responseJSON.UserId;
                            $.ajax({
                                url: "/UserProfile/GetUserProfilesByUserId",
                                type: "Post",
                                data: {
                                    userId: userId
                                },
                                statusCode: {
                                    200: function (response) {
                                        userProfile = response;
                                        setUserProfileSettings(userProfile);
                                    },
                                    204: function () {
                                        console.warn("The current user didn't have a profile data. A new Profile data will be created");
                                        var kendoColumns = [];
                                        $.each($grid.getOptions().columns, function () {
                                            var column = this;

                                            kendoColumns.push({
                                                Field: column.field,
                                                Width: column.width
                                            });
                                        });

                                        $.ajax({
                                            url: "/UserProfile/CreateUserProfile",
                                            type: "Post",
                                            data: {
                                                request: {
                                                    UserId: userId,
                                                    ItemsPerPage: $grid.dataSource.pageSize(),
                                                    VisibleColumns: getColumnMultiselectValue(),
                                                    Columns: kendoColumns
                                                }
                                            },
                                            success: function (response) {
                                                userProfile = response;

                                                setUserProfileSettings(userProfile);
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    });
                }

                function initOptionsDeleteRecord(selectedRows) {
                    var optionsDeleteRecord = {
                        modalId: "deleteRecordsModal",
                        modalTitle: "DELETE RECORDS",
                        mainButtonText: "Delete Records",
                        containsDynamicForm: "delete",
                        contentBody: selectedRows.map(a => a.RecordName),
                        mainButtonHandler: function () {
                            var result = selectedRows.map(a => a.Id);
                            var dataObj = {
                                Ids: result
                            };
                            $.ajax({
                                url: "/Record/DeleteManyAsync",
                                type: "POST",
                                data: dataObj,
                                complete: function (response) {
                                    $(".plugin-container").children().remove();
                                    $("body").removeClass("modal-open");
                                    $(".mdc-select__menu ").remove();
                                    $grid.dataSource.read();
                                }
                            });
                        },
                        cancelButtonHandler: function () { }
                    };

                    return optionsDeleteRecord;
                }

                function initOptionsArchiveRecord(selectedRows) {
                    var optionsArchiveRecord = {
                        modalId: "archiveRecordsModal",
                        modalTitle: "ARCHIVE RECORDS",
                        mainButtonText: "Archive Records",
                        containsDynamicForm: "archive",
                        contentBody: selectedRows.map(a => a.RecordName),
                        mainButtonHandler: function () {
                            var result = selectedRows.map(a => a.Id);
                            var dataObj = {
                                Ids: result,
                                Archive: true
                            };
                            $.ajax({
                                url: "/Record/ArchiveAsync",
                                type: "POST",
                                data: dataObj,
                                complete: function (response) {
                                    $(".plugin-container").children().remove();
                                    $("body").removeClass("modal-open");
                                    $(".mdc-select__menu ").remove();
                                    $grid.dataSource.read();
                                }
                            });
                        },
                        cancelButtonHandler: function () { }
                    };

                    return optionsArchiveRecord;
                }

                function onModalContentLoaded() {
                    $(".facebook-loading-animation-wrapper").hide();
                    $("#controlsContainer").css("display", "block");
                }

                function _createDataForClone(recordResponse) {

                    var recordTitles = [{
                        title: "HOUSEHOLDS",
                        items: generalHelperFunctions.parseArray(cloneHelperFunctions.createListForHousehold(recordResponse.Household, "Name"))
                    },
                    {
                        title: "APPLICANTS",
                        items: generalHelperFunctions.parseArray(cloneHelperFunctions.createListForApplicants(recordResponse.Person))
                    },
                    {
                        title: "INCOME",
                        items: generalHelperFunctions.parseArray(cloneHelperFunctions.createListForIncome(recordResponse.Income, recordResponse.Person))
                    },
                    {
                        title: "LIABILITIES",
                        items: generalHelperFunctions.parseArray(cloneHelperFunctions.createListForLiability(recordResponse.Liability))
                    },
                    {
                        title: "EXPENSES",
                        items: generalHelperFunctions.parseArray(cloneHelperFunctions.createListForExpenses(recordResponse.Expenses))
                    },
                    {
                        title: "LOANS",
                        items: generalHelperFunctions.parseArray(cloneHelperFunctions.createListForLoan(recordResponse.NewLoan))
                    }
                    ];

                    var $cloneContainer = $("<div></div>", { id: "cloneId" });
                    $("#controlsContainer").append($cloneContainer);

                    var viewModel = {
                        message: "The following information will be copied:",
                        recordTitles: recordTitles
                    };

                    ko.applyBindingsToNode($cloneContainer[0], {
                        template: {
                            name: "clone-template",
                            data: viewModel
                        }
                    });

                    const textField = [].map.call($("#cloneRecordName"), function (el) {
                        mdc.textField.MDCTextField.attachTo(el);
                    });


                    onModalContentLoaded();
                    $("#inputCloneRecord").focus();
                }

                function _createModalForMessages(options, modalMessages) {
                    var modalOptions = {
                        $container: $(".plugin-container"),
                        $modalContainer: $("#controlsContainer"),
                        template: "slideModalTemplate",
                        modalId: options.modalId,
                        title: options.modalTitle,
                        mainButtonText: options.mainButtonText,
                        mainButtonHandler: options.mainButtonHandler,
                        onSetContent: function () {
                            _displayModalRecordsMessage(options.contentBody, modalMessages, onModalContentLoaded);
                        },
                        onDestroyContent: function () {

                        }
                    };

                    var slideModal = new SlideModal(modalOptions);
                    slideModal.showModal();
                }

                function _createModalForClone(options, response) {
                    var modalOptions = {
                        $container: $(".plugin-container"),
                        $modalContainer: $("#controlsContainer"),
                        template: "slideModalTemplate",
                        modalId: options.modalId,
                        title: options.modalTitle,
                        mainButtonText: options.mainButtonText,
                        mainButtonHandler: options.mainButtonHandler,
                        onSetContent: function () {
                            _createDataForClone(response);
                        },
                        onDestroyContent: function () {
                            //this.jsonForms.destroy();
                        }
                    };

                    var slideModal = new SlideModal(modalOptions);
                    slideModal.showModal();
                }

                function _createModalForCreate() {

                    var modalOptions = {
                        $container: $(".plugin-container"),
                        $modalContainer: $("#controlsContainer"),
                        template: "slideModalTemplate",
                        modalId: "createRecordModal",
                        title: "CREATE RECORD",
                        mainButtonText: "Create Record",
                        mainButtonHandler: function () { },
                        onSetContent: function () {
                            ExpressionEngine.Utils.generateRestrictionRulesForMany(createRecordJson);

                            this.jsonForms = _createForm({
                                controls: createRecordJson,
                                record: {},
                                onSaveResultData: function (resultData, name) {
                                    //gets triggered every time a field is modified
                                    //send them to server - resultData
                                    myData = resultData;
                                }
                            });

                            $("#create-record-button").on("click", function () {
                                var householdData = JSON.parse(JSON.stringify(myData.Household));
                                householdData.Dependant = householdData.Dependant ? [householdData.Dependant] : [];

                                var obj = {
                                    Name: myData.RecordName,
                                    Person: JSON.stringify(myData.Person),
                                    Household: JSON.stringify(householdData)
                                };

                                $loader.show();

                                $.ajax({
                                    url: "/Record/CreateAsync",
                                    type: "POST",
                                    data: obj,
                                    success: function (response) {
                                        $loader.show();
                                        window.open(response.Url, "_self");
                                    },
                                    error: function () {
                                        $loader.fadeOut();
                                        alert("A problem occured");
                                    }
                                });
                            });
                        },
                        onDestroyContent: function () {
                            this.jsonForms.destroy();

                        }
                    };

                    var slideModal = new SlideModal(modalOptions);

                    if (self.areFormResourcesLoaded === false) {
                        setTimeout(function () {
                            self.areFormResourcesLoaded = true;
                            slideModal.showModal();
                        }, 450);
                    } else {
                        slideModal.showModal();
                    }
                }

                function _createForm(options) {

                    var jsonForm = new JsonForm({
                        $container: $("#controlsContainer"),
                        name: options.name,
                        controls: options.controls,
                        uniqueId: options.uniqueId,
                        onSaveResultData: options.onSaveResultData,
                        onControlCreated: function (control) {
                            setTimeout(function () {
                                $("#controlsContainer input:visible").eq(0).focus();
                            });
                        },
                        onFormCreated: function () {
                            $(".facebook-loading-animation-wrapper").hide();
                            $("#controlsContainer").css("display", "block");
                        },
                        record: options.record,
                        isDataCreated: true
                    });


                    setTimeout(function () {
                        $('#controlsContainer :input:first').focus();

                        $('#controlsContainer :input:last').on('keydown', function (e) {
                            if ($("this:focus") && e.which === 9) {
                                e.preventDefault();
                                $('#closeId').focus();
                            }
                        });
                    }, 900);

                    return jsonForm;
                }

                function _displayModalRecordsMessage(records, modalMessage, callback) {
                    var $messageContainer = $("<div></div>", { id: "messageId" });
                    $("#controlsContainer").append($messageContainer);

                    var viewModel = {
                        mainMessage: modalMessage.mainMessage,
                        items: records,
                        question: modalMessage.question
                    };

                    ko.applyBindingsToNode($messageContainer[0], {
                        template: {
                            name: "message-template",
                            data: viewModel
                        }
                    });

                    callback();
                }
            });
    });