﻿define(["jsonForm/Utilities/controlTypes",
    "jsonForm/StructuralControls/StructuralControls/ShowHideButtonControl",
    "jsonForm/StructuralControls/StructuralControls/SubtitleControl",
    "jsonForm/StructuralControls/StructuralControls/TabControl",
    "jsonForm/StructuralControls/StructuralControls/TitleControl",
    "jsonForm/StructuralControls/StructuralControls/GroupControl"
],
    function (controlTypes,
        ShowHideButtonControl,
        SubtitleControl,
        TabControl,
        TitleControl,
        GroupControl) {
        var structuralControlsFactory = {};

        structuralControlsFactory.createStructuralControl = function (controlType, controlOptions, callback) {
            switch (controlType) {
                case controlTypes.ShowHideButtonControl:
                    {
                        var control = new ShowHideButtonControl(controlOptions);
                        callback(control, controlOptions);
                        break;
                    }
                case controlTypes.SubtitleControl:
                    {
                        var control = new SubtitleControl(controlOptions);
                        callback(control, controlOptions);
                        break;
                    }
                case controlTypes.TabControl:
                    {
                        var control = new TabControl(controlOptions);
                        callback(control, controlOptions);
                        break;
                    }

                case controlTypes.TitleControl:
                    {
                        var control = new TitleControl(controlOptions);
                        callback(control, controlOptions);
                        break;
                    }

                case controlTypes.GroupControl:
                    {
                        var control = new GroupControl(controlOptions);
                        callback(control, controlOptions);
                        break;
                    }

                default:
                    alert("UNDEFINED STRUCTURAL CONTROL TYPE");
                    break;
            }
        };

        return structuralControlsFactory;
    });