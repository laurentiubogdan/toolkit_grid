define(["jquery", "knockout"], function ($, ko) {
    var _counter = 0;
    function _generateContainerId(index) {
        return "control-" + index;
    };

    function _generateInputId(index) {
        return "control-input-" + index;
    };



    function Control() {
        this.containerId = _generateContainerId(_counter);
        this.inputId = _generateInputId(_counter);
        _counter++;

        this.renderCustomTemplate = function ($container, templateName, options) {
            ko.applyBindingsToNode($container[0], { template: { name: templateName, data: options } });

            $container.find("[data-bind]").removeAttr("data-bind");
        };

        this.setValue = function () {
            console.warn("setValue() is not implemented for " + this.containerId);
        };

        this.getDomContainer = function () {
            var containerId = this.containerId;

            return $("#" + containerId);
        };

        this.getDomInput = function () {
            var inputId = this.inputId;

            return $("#" + inputId);
        };

        this.getValue = function () {
            var $control = this.getDomInput();

            return $control.val();
        };

        this.destroy = function (callback) {
            var $container = this.getDomContainer();
            $container.parent().remove();

            if (callback) {
                callback();
            }
        };

        this.setIsEnabled = function (isEnabled) {
            switch (this.options.type) {
                case "kendodropdown":
                    $("#" + this.inputId).data("kendoComboBox").enable(isEnabled);
                    break;

                case "date":
                    $("#" + this.inputId).data("kendoDatePicker").enable(isEnabled);
                    break;

                default:
                    var $control = $("#" + this.inputId);
                    //var $controlParent = $control.closest(".mdc-form-field");
                    //if we add or remove that class, the CSS is out of our control
                    if (isEnabled) {
                        $control.removeAttr("disabled");
                        //$controlParent.removeClass("mdc-text-field--disabled");
                    }
                    else {
                        $control.attr("disabled", "disabled");
                        //$controlParent.addClass("mdc-text-field--disabled");
                    }

                    break;
            }
        }

        this.bindEvents = function () {
            //add any events you want
            var control = this;

            //binding the change event
            switch (this.options.type) {
                case "checkbox":
                    var $control = $("input[name='" + this.options.name + "']"),
                        controlOptions = this.options;

                    $control.on("change", function () {
                        var isChecked = $(this).is(":checked"),
                            value = "";

                        if (isChecked) {
                            value = controlOptions.options[0].checkedValue;
                        } else {
                            value = controlOptions.options[0].uncheckedValue;
                        }

                        control.options.onValueChanged.apply(control, [value]);
                    });
                    break;

                case "radio":
                    var $control = $("input[name='" + this.inputId + "']"),
                        self = this;

                    $control.on("change", function () {
                        var value = $("input[name='" + self.inputId + "']:checked").val();

                        control.options.onValueChanged.apply(control, [value]);
                    });
                    break;

                case "kendonumberinput":
                case "leftLabel":
                    var kendoControl = $("#" + this.inputId).data("kendoNumericTextBox");

                    kendoControl.bind("change", function () {
                        var value = this.value();

                        control.options.onValueChanged.apply(control, [value]);
                    });
                    break;

                case "kendodropdown":
                    var kendoControl = $("#" + this.inputId).data("kendoComboBox");

                    kendoControl.bind("change", function () {
                        var dataItem = this.dataItem();
                        var value = this.value();

                        if (typeof dataItem === "undefined") {
                            value = "";
                        }

                        control.options.onValueChanged.apply(control, [value]);
                    });
                    break;

                case "date":
                    var kendoControl = $("#" + this.inputId).data("kendoDatePicker");
                    kendoControl.bind("change", function () {
                        var valueToSave = this.value();

                        if (!valueToSave || valueToSave === "") {
                            valueToSave = "";
                        }
                        else {
                            var userTimezoneOffset = valueToSave.getTimezoneOffset() * 60000;
                            valueToSave = new Date(valueToSave.getTime() - userTimezoneOffset);
                            //valueToSave
                            //valueToSave = valueToSave.toLocaleDateString("en-GB");
                        }

                        //console.log(valueToSave);

                        control.options.onValueChanged.apply(control, [valueToSave]);
                    });
                    break;

                default:
                    var $control = $("#" + this.inputId);
                    $control.on("change", function () {
                        var value = $(this).val();

                        control.options.onValueChanged.apply(control, [value]);
                    });
            }
        };

        this.setFloatingLabelClass = function (value) {
            var controlLabelId = this.inputId + "-labelId";
            var $controlInput = $("#" + this.inputId);
            var floatingLabelClass = "mdc-floating-label--float-above";
            var disabledColorClas = "disabled-color";
            var $labelInput = $("#" + controlLabelId);

            if ($labelInput) {
                if (value || value === 0) {
                    $labelInput.addClass(floatingLabelClass);
                    if ($controlInput.is(':disabled')) {
                        $controlInput.addClass(disabledColorClas);
                    }
                } else {
                    $labelInput.removeClass(floatingLabelClass);
                    if (!$controlInput.is(':disabled')) {
                        $controlInput.removeClass(disabledColorClas);
                    }
                }
            }
        };
    }

    return Control;
});