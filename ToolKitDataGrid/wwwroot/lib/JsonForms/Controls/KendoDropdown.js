define(["jquery", "jsonForm/Controls/Control", "jsonForm/Utilities/templateTypes", "jsonForm/Utilities/optionsHelpers", "material", "kendo.combobox.min"], //"kendo.datepicker.min", "kendo.combobox.min"
    function($, Control, templateTypes, optionsHelpers, mdc) {
        function KendoDropdownInputControl(options) {
            var self = this;
            Control.call(this);

            this.options = $.extend(true, {}, KendoDropdownInputControl.options, options);

            this.renderCustomTemplate(this.options.$parent, templateTypes.KendoDropdownInput, {
                containerId: this.containerId,
                inputId: this.inputId,
                label: this.options.label,
                placeholder: this.options.placeholder,
                name: this.options.name,
                inputSelectId: this.inputId + "-selectId",
                labelId: this.inputId + "-labelId",
                rippleId: this.inputId + "-rippleId",
                options: (this.options.formattedReferencedOptions !== undefined) ? this.options.formattedReferencedOptions : optionsHelpers.format(this.options.options),
                multiple: this.options.multiple,
                width: this.options.style.width,
                marginRight: this.options.style.marginRight,
                leftLabelVisible: this.options.style.additionalLeftLabel.visible,
                leftLabelText: this.options.style.additionalLeftLabel.text,
                leftLabelMargin: this.options.style.additionalLeftLabel.margin,
                leftLabelFontSize: this.options.style.additionalLeftLabel.fontSize,
                rightLabelVisible: this.options.style.additionalRightLabel.visible,
                rightLabelText: this.options.style.additionalRightLabel.text,
                rightLabelMargin: this.options.style.additionalRightLabel.margin,
                rightLabelFontSize: this.options.style.additionalRightLabel.fontSize,
                iconType: ((this.options.style.icon.leading === true) && (this.options.style.icon.trailing === false)) ? "mdc-select--with-leading-icon" : (((this.options.style.icon.leading === false) && (this.options.style.icon.trailing === true)) ? "mdc-select--with-trailing-icon" : ""),
                iconVisible: ((this.options.style.icon.leading === true) || (this.options.style.icon.trailing === true)) ? true : false,
                iconClass: this.options.style.icon.class,
                repeatableIndex: this.options.repeatableElement ? this.options.repeatableElement.index : ""
            });

            // var a = $("#" + this.inputId).children().addClass("dropdown-menu");

            //var $kendoDropdownId = $("#" + this.inputId);

            //$kendoDropdownId.on("change", function () {
            //    var value = $(this).val();
            //    self.options.onValueChanged.apply(self, [value]);
            //    console.log(value);
            //});

            //$("body").on("click", "#" + this.inputId + ".k-clear-value", function () {
            //    $("#" + self.containerId + " .mdc-floating-label").removeClass("mdc-floating-label--float-above");
            //    $("#" + self.containerId + " .mdc-floating-label").addClass("grey-label");
            //    $("#" + self.containerId + " .mdc-line-ripple").removeClass("mdc-line-ripple--active");
            //});

            //if ((record !== {}) && (uniqueId !== "")) {
            //    var valueArray = this.options.name.split(/[.]/);

            //    valueArray.shift();
            //    var targetValue = record[valueArray[0]];

            //    var targetObject = targetValue.find(obj => obj.UniqueID == uniqueId);

            //    for (var i = 1, len = valueArray.length; i < len; i++) {
            //        targetValue = targetObject[valueArray[i]];
            //    }

            //    this.setValue(targetValue);
            //} else {
            //    this.setValue(this.options.value);
            //}

            var control = this;
            var wrapper = $("#" + control.containerId).parent().attr("id");
            var backgroundFocusedClass = "mdc-ripple-upgraded--background-focused mdc-text-field--focused mdc-ripple-fg-upgraded--background-focused";
            var floatingLabelClass = "mdc-floating-label--float-above";
            var rippleClass = "mdc-line-ripple--active";
            var $labelId = $("#" + control.inputId + "-labelId");
            var $rippleId = $("#" + control.inputId + "-rippleId");
            var $dropdownInput = $("#" + control.inputId);

            //temp commented
            $("#" + wrapper).on({
                focusin: function() {
                    $(this).addClass(backgroundFocusedClass);
                    $labelId.addClass(floatingLabelClass);
                    $rippleId.addClass(rippleClass);
                },
                focusout: function() {
                    var $inputValue = $dropdownInput.val();
                    if ($inputValue === null || $inputValue === undefined) {
                        $labelId.removeClass(floatingLabelClass);
                    }
                    $(this).removeClass(backgroundFocusedClass);
                    $rippleId.removeClass(rippleClass);
                },
                keydown: function() {
                    var $inputValue = $dropdownInput.val();
                    if ($inputValue !== null || $inputValue !== undefined) {
                        $(this).addClass(backgroundFocusedClass);
                        $labelId.addClass(floatingLabelClass);
                        $rippleId.addClass(rippleClass);
                    }
                }
            });

            $dropdownInput.on("change", function () {
                var $inputValue = $dropdownInput.val();
                if ($inputValue === null || $inputValue === undefined) {
                    $labelId.removeClass(floatingLabelClass);
                    $(this).removeClass(backgroundFocusedClass);
                    $rippleId.removeClass(rippleClass);
                } else {
                    $(this).addClass(backgroundFocusedClass);
                    $labelId.addClass(floatingLabelClass);
                    $rippleId.addClass(rippleClass);
                }
            });

            const textField = [].map.call($("#" + wrapper), function(el) {
                mdc.textField.MDCTextField.attachTo(el);
            });

            //binding kendo here
            var $input = $("#" + this.inputId);
            var $containerId = $("#" + this.containerId);
            $containerId.parent().css("padding-right", this.options.style.paddingRight);

            $input.kendoComboBox({
                dataTextField: "text",
                dataValueField: "value",
                filter: "contains",
                highlightFirst: false
            });

            this.setValue = function(value, isValueChangedEventAvoided) {
                var kendoControl = $("#" + this.inputId).data("kendoComboBox");

                if (!value)
                    value = '';

                kendoControl.value(value);

                this.options.onValueSet(this);
                this.options.onValueChanged.apply(this, [value, isValueChangedEventAvoided]);
            };

            if ((this.options.formattedReferencedOptions !== undefined) && (this.options.formattedReferencedOptions === null)) {
                this.setValue("", true);
            } else {
                this.setValue(this.options.value, true);
            }

            var $inputValue = $input.val();

            if ($inputValue === null) {
                $("#" + this.inputId + "-labelId").removeClass(floatingLabelClass);
            }

            this.destroy = function() {
                var combobox = $("#" + this.inputId).data("kendoComboBox");
                combobox.destroy();

                var $container = this.getDomContainer();
                $container.parent().remove();
            };

            //delete value that is not in the list
            var kendoDropdown = $input.data("kendoComboBox");

            kendoDropdown.bind("change", function() {
                var dataItem = this.dataItem();

                if (typeof dataItem === "undefined") {
                    this.value("");
                }
            });

            //this.bindEvents();
        };

        KendoDropdownInputControl.options = {
            $parent: $({}),
            label: "Dropdown input",
            name: "",
            placeholder: "",
            value: "",
            options: [{
                text: "Default Text",
                value: "Default Value"
            }],
            multiple: false,
            rules: {},
            type: "kendodropdown",
            style: {
                width: "100%",
                marginRight: "0px",
                marginTop: "10px",
                additionalLeftLabel: {
                    visible: false,
                    text: "",
                    margin: "",
                    fontSize: ""
                },
                additionalRightLabel: {
                    visible: false,
                    text: "",
                    margin: "",
                    fontSize: ""
                },
                icon: {
                    leading: false,
                    trailing: false,
                    class: ""
                }
            },
            onValueSet: function() {},
            onValueChanged: function() {}
        };

        return KendoDropdownInputControl;
    });