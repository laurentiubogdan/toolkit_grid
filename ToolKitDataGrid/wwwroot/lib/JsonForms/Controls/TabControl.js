define(["jquery", "jsonForm/Controls/Control", "jsonForm/Utilities/templateTypes"], function ($, Control, templateTypes) {
    function TabControl(options) {
        Control.call(this);
        this.options = $.extend(true, {}, TabControl.options, options);

        this.renderCustomTemplate(this.options.$parent, templateTypes.TabControl, {
            containerId: this.containerId,
            text: this.options.text,
            name: this.options.name,
            width: this.options.style.width,
            marginLeft: this.options.style.marginLeft,
            marginTop: this.options.style.marginTop,
            fontSize: this.options.style.fontSize,
            fontWeight: this.options.style.fontWeight,
            color: this.options.style.color,
            tabs: this.options.tabs
        });
    };

    TabControl.options = {
        $parent: $({}),
        text: "TabControl",
        type: "tab",
        name: "",
        rules: {},
        tabs: [
            {
                tabText: "",
                tabControls: []
            }
        ],
        additionalRightLabelText: null,
        onValueSet: function () { }
    };

    return TabControl;
});