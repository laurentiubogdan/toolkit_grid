define(["jquery", "jsonForm/Controls/Control", "jsonForm/Utilities/templateTypes", "jsonForm/Utilities/optionsHelpers"], function ($, Control, templateTypes, optionsHelpers) {
    function CheckboxInputControl(options) {
        var self = this;
        Control.call(this);

        this.options = $.extend(true, {}, CheckboxInputControl.options, options);

        var formattedOptions = this.options.options;

        for (var i = 0, len = formattedOptions.length; i < len; i++) {
            formattedOptions[i]["uid"] = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        }

        this.renderCustomTemplate(this.options.$parent, templateTypes.CheckboxInput, {
            containerId: this.containerId,
            inputId: this.inputId,
            label: this.options.label,
            labelFontSize: this.options.style.labelFontSize,
            name: this.options.name,
            options: this.options.options,
            repeatableIndex: this.options.repeatableElement ? this.options.repeatableElement.index : ""
        });

        this.getValue = function () {
            var $controlContainer = this.getDomContainer();
            var checked = $controlContainer.find("input:checked").map(function () {
                return $(this).val();
            }).get();

            return checked;
        };

        //this.setValue = function (values, isValueChangedEventAvoided) {
        //    var $element = this.getDomContainer();
        //    console.log(values);


        //    if (values) {
        //        for (var i = 0, len = values.length; i < len; i++) {
        //            $element.find("input[type='checkbox'][value='" + values[i] + "']").prop("checked", "true");
        //        }
        //    }

        //    this.options.onValueSet(this);
        //    this.options.onValueChanged.apply(this, [values, isValueChangedEventAvoided]);
        //};

        this.setValue = function (value, isValueChangedEventAvoided) {
            var $element = this.getDomContainer();

            if (value) {
                $element.find("input[type='checkbox'][value='" + value + "']").prop("checked", "true");
            }

            this.options.onValueSet(this);
            this.options.onValueChanged.apply(this, [value, isValueChangedEventAvoided]);
        };

        if (!this.options.value || this.options.value === "") {
            this.options.value = this.options.options[0].uncheckedValue;
        }

        this.setValue(this.options.value, true);

        if (this.options.lastFormInput) {
            $("#" + this.inputId).on("keydown", function (e) {
                if (e.keyCode === 9) {
                    e.preventDefault();
                    $(".close-modal-button").first().focus();
                }
            });
        }

        //this.bindEvents();
    };

    CheckboxInputControl.options = {
        template: "CheckboxInputControl-template",
        label: "Checkbox Control",
        name: "",
        rules: {},
        type: "checkbox",
        options: [{
            text: "Default Text",
            checkedValue: "Default Value",
            uncheckedValue: "No",
            checked: true
        }],
        lastFormInput: false,
        $parent: $({}),
        style: {
            marginTop: "10px",
            labelFontSize: "16px"
        },
        onValueSet: function () { },
        onDestroy: function () { },
        onValueChanged: function () { }
    };

    return CheckboxInputControl;
});

