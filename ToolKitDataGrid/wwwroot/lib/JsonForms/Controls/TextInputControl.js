define(["jquery", "jsonForm/Controls/Control", "jsonForm/Utilities/templateTypes"], function ($, Control, templateTypes) {
    function TextInputControl(options) {
        var self = this;
        Control.call(this);
        this.options = $.extend(true, {}, TextInputControl.options, options);

        this.renderCustomTemplate(this.options.$parent, templateTypes.TextInput, {
            containerId: this.containerId,
            inputId: this.inputId,
            label: this.options.label,
            labelId: this.inputId + "-labelId",
            placeholder: this.options.placeholder,
            type: this.options.type,
            name: this.options.name,
            width: this.options.style.width,
            marginRight: this.options.style.marginRight,
            marginTop: this.options.style.marginTop,
            paddingRight: this.options.style.paddingRight,
            leftLabelVisible: this.options.style.additionalLeftLabel.visible,
            leftLabelText: this.options.style.additionalLeftLabel.text,
            leftLabelMargin: this.options.style.additionalLeftLabel.margin,
            leftLabelFontSize: this.options.style.additionalLeftLabel.fontSize,
            rightLabelVisible: this.options.style.additionalRightLabel.visible,
            rightLabelText: this.options.style.additionalRightLabel.text,
            rightLabelMargin: this.options.style.additionalRightLabel.margin,
            rightLabelFontSize: this.options.style.additionalRightLabel.fontSize,
            iconType: ((this.options.style.icon.leading === true) && (this.options.style.icon.trailing === false)) ? "mdc-text-field--with-leading-icon" : (((this.options.style.icon.leading === false) && (this.options.style.icon.trailing === true)) ? "mdc-text-field--with-trailing-icon" : ""),
            iconVisible: ((this.options.style.icon.leading === true) || (this.options.style.icon.trailing === true)) ? true : false,
            iconClass: this.options.style.icon.class,
            hasTooltip: this.options.tooltip.display,
            tooltipText: this.options.tooltip.text,
            tooltipPlacement: this.options.tooltip.dataPlacement,
            repeatableIndex: this.options.repeatableElement ? this.options.repeatableElement.index : ""
        });

        this.setValue = function (value, isValueChangedEventAvoided) {
            var $control = this.getDomInput();
            $control.val(value);

            this.options.onValueSet(this);
            this.options.onValueChanged.apply(this, [value, isValueChangedEventAvoided]);
        };

        this.setValue(this.options.value, true);

        //$("#" + this.inputId).on("change", function() {
        //    var value = $(this).val();
        //    self.options.onValueChanged.apply(self, [value]);
        //});

        if (this.options.tooltip.display) {
            $("#" + this.containerId).tooltip({
                trigger: this.options.tooltip.trigger
            });
        }

        $("#" + this.inputId).on("change", function () {
            var value = $(this).val();
            value = $.trim(value);
            $(this).val(value);
        })


        // if ((record !== {}) && (uniqueId !== "")) {
        //     var valueArray = this.options.name.split(/[.]/);

        //     valueArray.shift();
        //     var targetValue = record[valueArray[0]];

        //     var targetObject = targetValue.find(obj => obj.UniqueID == uniqueId);

        //     for (var i = 1, len = valueArray.length; i < len; i++) {
        //         targetValue = targetObject[valueArray[i]];
        //     }

        //     this.setValue(targetValue);
        // } else {
        //     this.setValue(this.options.value);
        // }

        //if (this.options.lastFormInput) {
        //    $("#" + this.inputId).on("keydown", function(e) {
        //        if (e.keyCode === 9) {
        //            e.preventDefault();
        //            $(".close-modal-button").first().focus();
        //        }
        //    });
        //}

        //this.bindEvents();
    };

    TextInputControl.options = {
        $parent: $({}),
        label: "Text input",
        name: "",
        placeholder: "",
        value: "",
        type: "text",
        lastFormInput: false,
        tooltip: {
            display: true,
            trigger: "hover",
            dataPlacement: "bottom",
            text: ""
        },
        rules: {},
        style: {
            width: "100%",
            marginRight: "0px",
            marginTop: "10px",
            paddingRight: "",
            additionalLeftLabel: {
                visible: false,
                text: "",
                margin: "",
                fontSize: ""
            },
            additionalRightLabel: {
                visible: false,
                text: "",
                margin: "",
                fontSize: ""
            },
            icon: {
                leading: false,
                trailing: false,
                class: ""
            }
        },
        additionalRightLabelText: null,
        onValueSet: function () { },
        onValueChanged: function () { }
    };

    return TextInputControl;
});