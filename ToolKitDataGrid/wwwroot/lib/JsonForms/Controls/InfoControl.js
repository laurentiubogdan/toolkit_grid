define(["jquery", "jsonForm/Controls/Control", "jsonForm/Utilities/templateTypes"], function ($, Control, templateTypes) {
    function InfoControl(options) {
        Control.call(this);
        this.options = $.extend(true, {}, InfoControl.options, options);

        this.renderCustomTemplate(this.options.$parent, templateTypes.InfoControl, {
            containerId: this.containerId,
            text: this.options.text,
            name: this.options.name,
            width: this.options.style.width,
            marginLeft: this.options.style.marginLeft,
            marginTop: this.options.style.marginTop,
            fontSize: this.options.style.fontSize,
            fontWeight: this.options.style.fontWeight,
            color: this.options.style.color
        });
    };

    InfoControl.options = {
        $parent: $({}),
        text: "Tooltip",
        rules: {},
        name: "",
        type: "info",
        style: {
            width: "100%",
            marginLeft: "0px",
            marginTop: "10px",
            fontSize: "20px",
            fontWeight: "600",
            color: "#004165"
        },
        additionalRightLabelText: null,
        onValueSet: function () { }
    };

    return InfoControl;
});