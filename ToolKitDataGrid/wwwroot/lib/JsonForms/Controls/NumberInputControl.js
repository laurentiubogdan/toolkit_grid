define(["jquery", "jsonForm/Controls/Control", "jsonForm/Utilities/templateTypes", "material"], function ($, Control, templateTypes, mdc) {
    function NumberInputControl(options) {
        var self = this;
        Control.call(this);
        this.options = $.extend(true, {}, NumberInputControl.options, options);

        this.renderCustomTemplate(this.options.$parent, templateTypes.NumberInput, {
            containerId: this.containerId,
            inputId: this.inputId,
            label: this.options.label,
            labelId: this.inputId + "-labelId",
            placeholder: this.options.placeholder,
            name: this.options.name,
            fieldType: this.options.fieldType,
            min: this.options.min,
            max: this.options.maximumValue,
            decimals: this.options.maximumDecimals,
            step: this.options.step,
            width: this.options.style.width,
            widthLabel: this.options.style.widthLabel,
            widthInput: this.options.style.widthInput,
            marginRight: this.options.style.marginRight,
            paddingRight: this.options.style.paddingRight,
            leftLabelVisible: this.options.style.additionalLeftLabel.visible,
            leftLabelText: this.options.style.additionalLeftLabel.text,
            leftLabelMargin: this.options.style.additionalLeftLabel.margin,
            leftLabelFontSize: this.options.style.additionalLeftLabel.fontSize,
            rightLabelVisible: this.options.style.additionalRightLabel.visible,
            rightLabelText: this.options.style.additionalRightLabel.text,
            rightLabelMargin: this.options.style.additionalRightLabel.margin,
            rightLabelFontSize: this.options.style.additionalRightLabel.fontSize,
            iconType: ((this.options.style.icon.leading === true) && (this.options.style.icon.trailing === false)) ? "mdc-text-field--with-leading-icon" : (((this.options.style.icon.leading === false) && (this.options.style.icon.trailing === true)) ? "mdc-text-field--with-trailing-icon" : ""),
            iconVisible: ((this.options.style.icon.leading === true) || (this.options.style.icon.trailing === true)) ? true : false,
            iconClass: this.options.style.icon.class,
            hasTooltip: this.options.tooltip.display,
            tooltipText: this.options.tooltip.text,
            tooltipPlacement: this.options.tooltip.dataPlacement,
            type: this.options.format.type,
            symbol: this.options.format.symbol,
            symbolPlacement: this.options.format.symbolPlacement,
            repeatableIndex: this.options.repeatableElement ? this.options.repeatableElement.index : ""
        });

        var hasIcon = $("#" + this.inputId).parent().hasClass("mdc-text-field--with-leading-icon");

        if (hasIcon) {
            $("#" + this.inputId).parent().find(".mdc-floating-label").css("left", "48px");
        }

        if (this.options.tooltip.display) {
            $("#" + this.containerId).tooltip({
                trigger: this.options.tooltip.trigger
            });
        }

        $("#" + this.inputId).keydown(function (event) {
            if (self.options.lastFormInput) {
                if (e.keyCode === 9) {
                    e.preventDefault();
                    $(".close-modal-button").first().focus();
                }
            }

            if (event.shiftKey === true) {
                event.preventDefault();
            }

            if ((event.keyCode >= 48 && event.keyCode <= 57) ||
                (event.keyCode >= 96 && event.keyCode <= 105) ||
                event.keyCode === 8 || event.keyCode === 9 || event.keyCode === 37 ||
                event.keyCode === 39 || event.keyCode === 46 || event.keyCode === 190 || event.keyCode === 110) {
                //nada
            } else {
                event.preventDefault();
            }

            if ($(this).val().indexOf('.') !== -1 && event.keyCode === 190)
                event.preventDefault();
            //if a decimal has been added, disable the "."-button
        });


        if (self.options.decimals === "") {
            $("#" + self.inputId).keydown(function (event) {
                if (event.keyCode === 190) {
                    return false;
                }
            });
        }

        this.setValue = function (value, isValueChangedEventAvoided) {
            var self = this;
            var $control = this.getDomInput();
            $control.val(value);

            this.options.onValueSet(this);
            this.options.onValueChanged.apply(this, [value, isValueChangedEventAvoided]);
        };

        const textField = [].map.call($("#" + this.containerId), function (el) {
            mdc.textField.MDCTextField.attachTo(el);
        });

        this.setValue(this.options.value, true);
        //this.bindEvents();
    };

    NumberInputControl.options = {
        $parent: $({}),
        label: "Number input",
        name: "",
        placeholder: "",
        value: "",
        type: "number",
        fieldType: "tel",
        min: "-999999999",
        max: "999999999",
        step: "1",
        decimals: "",
        lastFormInput: false,
        rules: {},
        tooltip: {
            display: true,
            trigger: "hover",
            dataPlacement: "bottom",
            text: ""
        },
        format: {
            type: "",
            symbol: "",
            symbolPlacement: ""
        },
        style: {
            width: "100%",
            widthLabel: "60%",
            widthInput: "40%",
            marginRight: "0px",
            paddingRight: "",
            marginTop: "10px",
            additionalLeftLabel: {
                visible: false,
                text: "",
                margin: "",
                fontSize: ""
            },
            additionalRightLabel: {
                visible: false,
                text: "",
                margin: "",
                fontSize: ""
            },
            icon: {
                leading: false,
                trailing: false,
                class: ""
            }
        },
        onValueSet: function () { },
        onValueChanged: function () { }
    };

    return NumberInputControl;
});