define([], function() {
    var templates = {
        TextInput: "TextInputControl-template",
        NumberInput: "NumberInputControl-template",
        KendoNumberInput: "KendoNumberInputControl-template",
        DateInput: "DateInputControl-template",
        TextareaInput: "TextareaInputControl-template",
        DropdownInput: "DropdownInputControl-template",
        RadioButton: "RadioButtonInputControl-template",
        CheckboxInput: "CheckboxInputControl-template",
        TitleControl: "TitleControl-template",
        SubtitleControl: "SubtitleControl-template",
        ShowHideButtonControl: "ShowHideButtonControl-template",
        InfoControl: "InfoControl-template",
        TabControl: "TabControl-template",
        KendoDropdownInput: "KendoDropDown-template",
        LeftLabelInputControl: "LeftLabelInputControl-template",
        GroupControl: "GroupControl-template"
    }

    return templates;
});