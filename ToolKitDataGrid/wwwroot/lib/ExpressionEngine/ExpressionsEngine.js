﻿define([], function () {
    function ExpressionsEngine(params) {
        //params is optional
        params = params || {};
        this.recordData = params.recordData;
    }

    ExpressionsEngine.prototype.updateRecordData = function (newRecordData) {
        this.recordData = newRecordData;
    };
    
    ExpressionsEngine.prototype.evaluateExpression = function (params) {
        var self = this;

        var expression = params.expression,
            expressionContextData = params.expressionContextData,
            contextName = params.contextName,
            repeatableElement = params.repeatableElement;

        var context = expressionContextData;
        if (!expressionContextData) {
            context = this.recordData;
        }

        var pieceToReplace = "record.";

        var updatedExpression = expression;
        var repeatableElementPath = null;
        if (repeatableElement) {
            repeatableElementPath = repeatableElement.path.replace(pieceToReplace, "");
            if (contextName) {
                repeatableElementPath = repeatableElementPath.replace(contextName + ".", "");
            }
        }

        var parameters = expression.match(/\[\#[0-9a-zA-Z' '_.-]*\#\]/g);
        if (parameters) {
            for (var i = 0; i < parameters.length; i++) {
                var parameter = parameters[i]
                    .replace(/\[/g, "")
                    .replace(/\]/g, "")
                    .replace(/#/g, "");

                parameter = parameter.replace(pieceToReplace, "");
                if (contextName) {
                    parameter = parameter.replace(contextName + ".", "");
                }

                var expressionContext = "context.";
                var foundParameter = ExpressionsEngine.Utils.findNestedParameter(context, parameter, repeatableElement);
                if (!foundParameter && foundParameter !== 0) {
                    var globalParameter = ExpressionsEngine.Utils.findNestedParameter(this.recordData, parameter, repeatableElement);
                    if (!globalParameter && foundParameter !== 0) {
                        updatedExpression = updatedExpression.replace(parameters[i], "null");
                        continue;
                    }

                    expressionContext = "globalContext.";
                }

                var expressionParameter = parameter;
                if (repeatableElement &&
                    parameter.indexOf(".") > 0) {
                    var propertyFirstPart = parameter.substring(0, parameter.indexOf("."));
                    if (propertyFirstPart === repeatableElementPath) {
                        expressionParameter = expressionParameter.replace(propertyFirstPart, propertyFirstPart + "[" + repeatableElement.index + "]");
                    }
                }

                updatedExpression = updatedExpression.replace(parameters[i], expressionContext + expressionParameter);
            }
        }

        var result = true;
        try {
            var calculateExpression = Function("context", "globalContext", updatedExpression);
            result = calculateExpression(context, this.recordData);
        }
        catch (err) {
            console.log("EXPRESSION FAILED: ", updatedExpression, context, err);
            result = true;
        } 

        return result;
    }; 

    ExpressionsEngine.prototype.validateReferenceField = function (controlOptions, value) {
        var result = {
            isValid: true,
            errorMessages: []
        };

        if (!controlOptions.referencedOptions ||
            !value ||
            value === '')
            return result;

        var sectionData = this.recordData[controlOptions.referencedOptions.entity];
        var foundItem = sectionData.find(function (element) {
            return element.UniqueID === value;
        });

        if (!foundItem) {
            result.isValid = false;
            result.errorMessages.push("Invalid reference value on " + controlOptions.label);
        }

        return result;
    };

    //UTILS
    ExpressionsEngine.Utils = {};

    ExpressionsEngine.Utils.generateRestrictionRules = function (controlOptions) {
        var result = [];

        var parameter = "[#" + controlOptions.name + "#]";
        var expression = "";
        var errorMessage = "";

        if (controlOptions.minimumLength &&
            controlOptions.minimumLength !== "") {
            expression = "if (!" + parameter + ") return true; return " + parameter + ".toString().length >= " + controlOptions.minimumLength + ";";
            errorMessage = controlOptions.label + " must have more than " + controlOptions.minimumLength + " characters";
            result.push({
                condition: expression,
                errorMessage: errorMessage
            });
        }

        if (controlOptions.maximumLength &&
            controlOptions.maximumLength !== "") {
            expression = "if (!" + parameter + ") return true; return " + parameter + ".toString().length <= " + controlOptions.maximumLength + ";";
            errorMessage = controlOptions.label + " must have less than " + controlOptions.maximumLength + " characters";
            result.push({
                condition: expression,
                errorMessage: errorMessage
            });
        }

        if (controlOptions.allowedCharacters &&
            controlOptions.allowedCharacters !== "") {

            var specialChars = "";
            if (controlOptions.allowedSpecialCharacters &&
                controlOptions.allowedSpecialCharacters !== "") {
                //
                specialChars = controlOptions.allowedSpecialCharacters;
            }

            var messageSpecialChars = specialChars;

            var regex = null;
            specialChars = specialChars.replace("'", "\\'").replace('"', '\\"');

            switch (controlOptions.allowedCharacters) {
                case "Any":
                    break;

                case "Alpha":
                    regex = "^[a-zA-Z" + specialChars + "]+$";
                    break;

                case "Numeric":
                    regex = "^[0-9" + specialChars + "]+$";
                    break;

                case "AlphaNumeric":
                    regex = "^[a-zA-Z0-9" + specialChars + "]+$";
                    break;

                default:
                    //maybe throw an error ?
                    break;
            }

            if (regex) {
                expression = "var item = " + parameter + "; if (!item) return true; var regex = new RegExp('" + regex + "', 'i'); return regex.test(item);";

                errorMessage = controlOptions.label + " can only contain " + controlOptions.allowedCharacters;
                if (specialChars !== "") {
                    errorMessage = errorMessage + " and " + messageSpecialChars;
                }
                errorMessage = errorMessage + " characters";

                result.push({
                    condition: expression,
                    errorMessage: errorMessage
                });
            }
        }

        if (controlOptions.minimumValue &&
            controlOptions.minimumValue !== "") {
            expression = "var item = " + parameter + "; if ((!item || item === '') && item !== 0) return true; return parseFloat(item) >= " + controlOptions.minimumValue + ";";
            errorMessage = controlOptions.label + " must be greater than " + controlOptions.minimumValue;
            result.push({
                condition: expression,
                errorMessage: errorMessage
            });
        }

        if (controlOptions.maximumValue &&
            controlOptions.maximumValue !== "") {
            expression = "var item = " + parameter + "; if ((!item || item === '') && item !== 0) return true; return parseFloat(item) <= " + controlOptions.maximumValue + ";";
            errorMessage = controlOptions.label + " must be less than " + controlOptions.maximumValue;
            result.push({
                condition: expression,
                errorMessage: errorMessage
            });
        }

        if (controlOptions.regularExpression &&
            controlOptions.regularExpression !== "") {
            expression = "var regex = new RegExp('" + controlOptions.regularExpression + "', 'i'); return regex.test(" + parameter + ");";
            errorMessage = controlOptions.label + " must match the following regex: " + controlOptions.regularExpression;

            result.push({
                condition: expression,
                errorMessage: errorMessage
            });
        }

        if (controlOptions.referencedOptions) {
            expression = "var items = [#record." + controlOptions.referencedOptions.entity + "#]; var value = " + parameter + "; if (!items || items.length === 0) { if (value && value !== '') return false; return true; } for (var i=0; i<items.length; i++) { if (items[i].UniqueID === value) return true; } return false;";
            errorMessage = controlOptions.label + " has an invalid reference value";
            result.push({
                condition: expression,
                errorMessage: errorMessage
            });
        }

        if (!controlOptions.rules) {
            controlOptions.rules = {};
        }

        controlOptions.rules.calculatedRestrictions = {
            conditions: result
        };
    };

    ExpressionsEngine.Utils.generateRestrictionRulesForMany = function (controls) {
        for (var i = 0; i < controls.length; i++) {
            var control = controls[i];

            if (controls[i].type === "Row" || 
                controls[i].type === "GroupControl") {
                this.generateRestrictionRulesForMany(control.options.controls);
                continue;
            }

            if (controls[i].type === "TabControl") {
                for (var j = 0; j < control.options.tabs.length; j++) {
                    this.generateRestrictionRulesForMany(control.options.tabs[j].controls);
                }
                continue;
            }

            this.generateRestrictionRules(control.options);
        }
    }

    ExpressionsEngine.Utils.findNestedParameter = function (context, property, repeatableElement) {
        var propertyFirstPart = property;

        if (property.indexOf(".") > 0) {
            propertyFirstPart = property.substring(0, property.indexOf("."));
            var propertyLastPart = property.replace(propertyFirstPart + ".", "");

            if (repeatableElement &&
                propertyFirstPart === repeatableElement.path &&
                context[propertyFirstPart]) {
                //the element[propertyFirstPart] is an array - we want the target property at an index
                return ExpressionsEngine.Utils.findNestedParameter(context[propertyFirstPart][repeatableElement.index], propertyLastPart, repeatableElement);
            }

            if (context[propertyFirstPart]) {
                return ExpressionsEngine.Utils.findNestedParameter(context[propertyFirstPart], propertyLastPart);
            }
        }

        return context[propertyFirstPart];
    };

    ExpressionsEngine.Utils.getTriggerFields = function (params) {
        var expression = params.expression;

        var result = [];

        var parameters = expression.match(/\[\#[0-9a-zA-Z' '_.-]*\#\]/g);
        if (!parameters) {
            return result;
        }

        for (var i = 0; i < parameters.length; i++) {
            var parameter = parameters[i]
                .replace(/\[/g, "")
                .replace(/\]/g, "")
                .replace(/#/g, "");

            if (result.indexOf(parameter) > 0)
                continue;

            result.push(parameter);
        }

        return result;
    };

    ExpressionsEngine.Utils.getGlobalDependenciesDictionary = function (params) {
        var controls = params.controls,
            sectionName = params.sectionName,
            resultDictionary = params.resultDictionary || {};

        for (var i = 0; i < controls.length; i++) {
            var control = controls[i];

            if (control.type === "Row" ||
                control.type === "GroupControl") {
                this.getGlobalDependenciesDictionary({
                    controls: control.options.controls,
                    sectionName: sectionName,
                    resultDictionary: resultDictionary
                });
            }

            if (control.type === "TabControl") {
                for (var j = 0; j < control.options.tabs.length; j++) {
                    this.getGlobalDependenciesDictionary({
                        controls: control.options.tabs[j].controls,
                        sectionName: sectionName,
                        resultDictionary: resultDictionary
                    });
                }
            }

            this.getGlobalDependenciesDictionaryForRules({
                rules: control.options.rules,
                sectionName: sectionName,
                resultDictionary: resultDictionary
            });

            //reference field type
            if (control.options.referencedOptions) {
                if (!resultDictionary[control.options.referencedOptions.entity]) {
                    resultDictionary[control.options.referencedOptions.entity] = {};
                }

                resultDictionary[control.options.referencedOptions.entity][sectionName] = true;
            }
        }
    }

    ExpressionsEngine.Utils.getGlobalDependenciesDictionaryForRules = function (params) {
        var rules = params.rules,
            sectionName = params.sectionName,
            resultDictionary = params.resultDictionary;

        if (!rules)
            return;

        for (var ruleType in rules) {
            if (rules[ruleType].type !== "On Condition") {//||
                //ruleType === "calculatedRestrictions") {
                continue;
            }

            //if (rules[ruleType].condition)

            //for multiple conditions
            var triggerFieldsForThisExpression = [];
            if (rules[ruleType].conditions) {
                for (var i = 0; i < rules[ruleType].conditions.length; i++) {
                    var triggerFieldsForExpression = ExpressionsEngine.Utils.getTriggerFields({
                        expression: rules[ruleType].conditions[i].condition
                    });

                    for (var j = 0; j < triggerFieldsForExpression.length; j++) {
                        if (triggerFieldsForThisExpression.indexOf(triggerFieldsForExpression[j]) > -1)
                            continue;

                        triggerFieldsForThisExpression.push(triggerFieldsForExpression[j]);
                    }
                }
            }
            else {
                triggerFieldsForThisExpression = this.getTriggerFields({
                    expression: rules[ruleType].condition
                });
            }

            for (var j = 0; j < triggerFieldsForThisExpression.length; j++) {
                var shortenedName = triggerFieldsForThisExpression[j].replace("record.", "");

                if (shortenedName.startsWith(sectionName))
                    continue;

                var triggererSection = shortenedName;
                if (triggererSection.indexOf('.') > -1) {
                    triggererSection = triggererSection.substring(0, triggererSection.indexOf('.'))
                }

                if (!resultDictionary[triggererSection]) {
                    resultDictionary[triggererSection] = {};
                }

                resultDictionary[triggererSection][sectionName] = true;
            }
        }
    }

    return ExpressionsEngine;
});