require.config({
    baseUrl: "/",
    paths: {
        "jquery": "./node_modules/jquery/dist/jquery.min",
        "bootstrap": "./node_modules/bootstrap/dist/js/bootstrap.bundle.min",
        "knockout": "./node_modules/knockout/build/output/knockout-latest",
        "knockout-mapping": "./node_modules/knockout-mapping/dist/knockout.mapping.min"
    },
    config: {
        text: {
            useXhr: function () {
                // allow cross-domain requests
                // remote server allows CORS
                return false;
            }
        }
    }
});