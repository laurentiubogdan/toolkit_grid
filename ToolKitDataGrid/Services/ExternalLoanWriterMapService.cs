﻿using System.Collections.Generic;
using System.Reflection;
using ToolKitGrid.ViewModels.Profile;

namespace ToolKitGrid.Services
{
    public static class ExternalLoanWriterMapService
    {
        public static IList<string> GetLockedFields(ExternalLoanWriterModel externalLoanWriterModel)
        {
            List<string> lockedFields = new List<string>();
            foreach (PropertyInfo propertyInfo in externalLoanWriterModel.GetType().GetProperties())
            {
                if (propertyInfo.GetValue(externalLoanWriterModel) != null
                    && !string.IsNullOrEmpty(propertyInfo.GetValue(externalLoanWriterModel).ToString()))
                {
                    lockedFields.Add(propertyInfo.Name);
                }
            }
            return lockedFields;
        }
    }
}
