﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ToolKitGrid.Services.Jwt
{
    public interface IJwtService
    {
        Task<ValidateJwtResponse> ValidateAsync(string jwt);
        IDictionary<string, object> GetJwtPayload(string jwt);
    }
}
