﻿using ToolKitGrid.Domain.Models.Identity;
using ToolKitGrid.ViewModels.Profile;

namespace ToolKitGrid.Services
{
    public static class LoanWriterMapService
    {
        public static LoanWriter MapExternalOnCurrent(ExternalLoanWriterModel externalLoanWriterModel, LoanWriter loanWriter)
        {
            loanWriter.AO = externalLoanWriterModel.AO;
            loanWriter.SAO = externalLoanWriterModel.SAO;
            loanWriter.LoanWriterFirstName = externalLoanWriterModel.FirstName;
            loanWriter.LoanWriterSurname = externalLoanWriterModel.Surname;
            loanWriter.LoanWriterEmail = externalLoanWriterModel.Email;
            loanWriter.Role = externalLoanWriterModel.Role;
            loanWriter.LoanWriterMobile.Number = externalLoanWriterModel.Mobile;
            loanWriter.LoanWriterAddress.Suburb = externalLoanWriterModel.Suburb;
            loanWriter.LoanWriterAddress.AustralianState = externalLoanWriterModel.State;
            loanWriter.LoanWriterAddress.Country = externalLoanWriterModel.Country;
            loanWriter.LoanWriterAddress.AustralianPostCode= externalLoanWriterModel.Postcode;
            return loanWriter;
        }
    }
}
