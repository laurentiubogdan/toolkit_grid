﻿namespace ToolKitGrid.Helpers.AWS
{
    public interface IAwsConfigurationFactory
    {
        AwsConfiguration Get();
    }
}
