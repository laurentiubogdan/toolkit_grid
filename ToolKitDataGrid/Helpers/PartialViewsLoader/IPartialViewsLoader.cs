﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Qualify.Web.Mvc.Helpers.PartialViewsLoader
{
    public interface IPartialViewsLoader
    {
        Task<string> RenderRazorViewToString(Controller controller, string partialView, object model, object additionalInfo = null);
        Task<string> GetViewPageHtml(Controller controller, string viewName, object model);
    }
}
