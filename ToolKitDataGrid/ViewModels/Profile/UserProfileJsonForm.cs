﻿namespace ToolKitGrid.ViewModels.Profile
{
    public class UserProfileJsonForm
    {
        public ExternalLoanWriterModel ExternalLoanWriterModel { get; set; }
        public string[] LockedFields { get; set; }
    }
}
