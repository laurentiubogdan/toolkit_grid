﻿using System;

namespace ToolKitGrid.ViewModels.Record
{
    public class GridRecordViewModel
    {
        public string Id { get; set; }
        public string UniqueId { get; set; }
        public string RecordName { get; set; }
        public string Applicants { get; set; }
        public string CreatedBy { get; set; }
        //public string UmiResult { get; set; }
        //public string MaxBorrowing { get; set; }
        //public DateTime? PricingRequestDate { get; set; }
        //public string PricingResult { get; set; }
        //public IEnumerable<string> Status { get; set; }
        public DateTime DateCreated { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime DateUpdated { get; set; }
        public string BDMName { get; set; }
    }
}

