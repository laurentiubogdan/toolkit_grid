﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using ToolKitGrid.Configuration;

namespace ToolKitGrid
{
    public static class AppSettings
    {
        public static IConfiguration Configuration { get; set; }
        public static IHostingEnvironment Environment { get; set; }
        public static bool SecurityEnabled => bool.Parse(Configuration[Constants.ApplySecurityConfigKey]);
        public static string DefaultLoginUserUniqueID => Configuration[Constants.DefaultLoginUserUniqueIDCoinfigKey];
        public static string PublicJsonS3Versions => Configuration[Constants.PublicJsonS3Versions];
        public static string EnvironmentBucket => Configuration[Constants.EnvironmentBucket];

    }
}
