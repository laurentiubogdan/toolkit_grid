﻿namespace ToolKitGrid.DTO
{
    public class CustomDate
    {
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
    }
}
