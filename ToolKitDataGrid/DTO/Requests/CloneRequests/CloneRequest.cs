﻿namespace ToolKitGrid.DTO.Requests.CloneRequests
{
    public class CloneRequest
    {
        public string RecordName { get; set; }
        public string UniqueID { get; set; }
    }
}
