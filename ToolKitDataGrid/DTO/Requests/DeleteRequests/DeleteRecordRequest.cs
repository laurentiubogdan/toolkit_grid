﻿using System.Collections.Generic;

namespace ToolKitGrid.DTO.Requests.DeleteRequests
{
    public class DeleteRecordRequest
    {
        public IEnumerable<string> Ids { get; set; }
    }
}
