﻿using System.Collections.Generic;
using ToolKitGrid.Domain.Models.Embedded;

namespace ToolKitGrid.DTO.Requests.CreateRequests
{
    public class RecordCreateRequest
    {
        public string Name { get; set; }
        //public Person Person { get; set; }
        //public Household Household { get; set; }

        public string Person { get; set; }
        public string Household { get; set; }
    }
}
