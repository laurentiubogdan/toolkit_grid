﻿using System.Collections.Generic;
using ToolKitGrid.Domain.HelperModels;

namespace ToolKitGrid.DTO.Requests.CreateRequests
{
    public class UserProfileCreateRequest
    {
        public string UserId { get; set; }
        public int ItemsPerPage { get; set; }
        public IEnumerable<KendoColumnWidth> Columns { get; set; }
        public IEnumerable<string> VisibleColumns { get; set; }
    }
}
