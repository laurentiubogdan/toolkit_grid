﻿using System.Collections.Generic;

namespace ToolKitGrid.DTO.Requests.ArchiveRequests
{
    public class ArchiveRecordRequest
    {
        public IEnumerable<string> Ids { get; set; }
        public bool Archive { get; set; }
    }
}
