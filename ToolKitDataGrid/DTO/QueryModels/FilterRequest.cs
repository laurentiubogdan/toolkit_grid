﻿using System.Collections.Generic;

namespace ToolKitGrid.DTO.QueryModels
{
    public class FilterRequest
    {
        public string Property { get; set; }
        public string Operator { get; set; }
        public List<string> Values { get; set; }
    }
}