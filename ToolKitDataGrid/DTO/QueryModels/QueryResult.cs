﻿using System.Collections.Generic;
using ToolKitGrid.Domain.Models;

namespace ToolKitGrid.DTO.QueryModels
{
    public class QueryResult
    {
        public long RecordsCount { get; set; }
        public IEnumerable<Record> Records { get; set; }
    }
}
