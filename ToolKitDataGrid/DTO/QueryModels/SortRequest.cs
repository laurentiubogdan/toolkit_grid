﻿namespace ToolKitGrid.DTO.QueryModels
{
    public class SortRequest
    {
        public string Field { get; set; }
        public string Direction { get; set; }
    }
}
