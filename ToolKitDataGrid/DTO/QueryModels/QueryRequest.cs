﻿using System.Collections.Generic;

namespace ToolKitGrid.DTO.QueryModels
{
    public class QueryRequest
    {
        public ProjectionRequest ProjectionRequest { get; set; }
        public List<FilterRequest> FilterRequests { get; set; }
        public List<SortRequest> SortRequests { get; set; }
        public int Limit { get; set; }
        public int Skip { get; set; }
    }
}