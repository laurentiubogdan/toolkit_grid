﻿using System.Collections.Generic;

namespace ToolKitGrid.DTO.QueryModels
{
    public class ProjectionRequest
    {
        public IEnumerable<string> ProjectionFields { get; set; }
    }
}
