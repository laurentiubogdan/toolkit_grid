﻿namespace ToolKitGrid.DTO.Responses
{
    public class UserSessionResponse
    {
        public UserSessionResponse()
        { }

        public bool IsValidSession { get; set; }

        public string Error { get; set; }
    }
}
