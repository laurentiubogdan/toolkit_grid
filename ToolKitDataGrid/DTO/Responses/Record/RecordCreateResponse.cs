﻿namespace ToolKitGrid.DTO.Responses.Record
{
    public class RecordCreateResponse
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int NumberOfHouseHolds { get; set; }
        public int NumberOfApplicants { get; set; }
    }
}
