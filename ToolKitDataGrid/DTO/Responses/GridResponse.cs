﻿using System.Collections.Generic;
using ToolKitGrid.ViewModels.Record;

namespace ToolKitGrid.DTO.Responses
{
    public class GridResponse
    {
        public long RecordsCount { get; set; }
        public IEnumerable<GridRecordViewModel> GridRecords { get; set; }
    }
}
