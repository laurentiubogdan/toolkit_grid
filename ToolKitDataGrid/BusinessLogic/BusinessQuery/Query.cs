﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToolKitGrid.Domain.Models;
using ToolKitGrid.DTO;
using ToolKitGrid.DTO.QueryModels;
using ToolKitGrid.Repository;
using ToolKitGrid.ViewModelExtensionMethods;

namespace ToolKitGrid.BusinessLogic.BusinessQuery
{
    public class Query : IQuery
    {
        private readonly IRecordRepository _recordRepository;
        //private readonly IGridRecordRepository _gridRecordRepository;


        public Query(IRecordRepository recordRepository)
        {
            _recordRepository = recordRepository;
            //_gridRecordRepository = gridRecordRepository;
        }

        public async Task<QueryResult> ListRecordsAsync(QueryRequest queryRequest, string uniqueId)

        {
            QueryResult result = new QueryResult();

            //SortDefinitionBuilder<Record> builder = Builders<Record>.Sort;
            //SortDefinition<Record> applicantFirstNameSort = builder.Descending("PersonApplicants.FirstName");
            //SortDefinition<Record> applicantFirstNameSort2 = builder.Descending("PersonApplicants.Surname");
            //SortDefinition<Record> adsa = builder.Combine(applicantFirstNameSort, applicantFirstNameSort2);

            //var baseQuery = _recordRepository.BaseQuery().Find(p => true).Sort(adsa);
            //result.RecordsCount = await _recordRepository.BaseQuery().CountDocumentsAsync(p => true);
            //result.Records = await baseQuery.ToListAsync();

            //return result;


            result.RecordsCount = await _recordRepository.BaseQuery().CountDocumentsAsync(p => p.CreatedByUniqueID == uniqueId);
            IFindFluent<Record, Record> baseQuery = _recordRepository.BaseQuery().Find(p => p.CreatedByUniqueID == uniqueId);
            if (queryRequest.FilterRequests != null && queryRequest.FilterRequests.Count() != 0)
            {
                FilterDefinitionBuilder<Record> builder = Builders<Record>.Filter;
                List<FilterDefinition<Record>> inFilterDefinitions = new List<FilterDefinition<Record>>();
                List<FilterDefinition<Record>> containsFilterDefinitions = new List<FilterDefinition<Record>>();

                foreach (FilterRequest request in queryRequest.FilterRequests)
                {
                    string property = request.Property;
                    string theOperator = request.Operator;

                    if (theOperator == "In")
                    {
                        FilterDefinition<Record> filterDefinition = builder.In(request.Property, request.Values);
                        inFilterDefinitions.Add(filterDefinition);
                    }
                    else if (theOperator == "Contains")
                    {
                        if (request.Property == "DateCreated" || request.Property == "Pricing.RequestDate" || request.Property == "DateUpdate")
                        {
                            CustomDate date = request.Values.First().GetDateFromString();

                            if (date.Day == 0 && date.Month == 0 && date.Year == 0)
                            {
                                continue;
                            }

                            DateTime fromDate = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, 0, DateTimeKind.Utc);
                            DateTime toDate = fromDate.AddDays(1).AddMilliseconds(-1);

                            FilterDefinition<Record> fromDateFilterDefinition = builder.Gte(request.Property, fromDate);
                            FilterDefinition<Record> toDateFilterDefinition = builder.Lte(request.Property, toDate);

                            FilterDefinition<Record> dateBetweenFilterDefinition = builder.And(fromDateFilterDefinition, toDateFilterDefinition);
                            containsFilterDefinitions.Add(dateBetweenFilterDefinition);
                        }
                        else if (request.Property == "Status")
                        {
                            FilterDefinition<Record> filterDefinition = builder.In(request.Property, request.Values);
                            inFilterDefinitions.Add(filterDefinition);
                        }
                        else if (property.Contains("&&"))
                        {
                            IEnumerable<string> fields = property.Split("&&");

                            foreach (string splitedField in fields)
                            {
                                property = splitedField;
                                AddFilterBuilder();
                            }

                            void AddFilterBuilder()
                            {
                                if (request.Values.First().Contains(" "))
                                {
                                    IEnumerable<string> words = request.Values.First().Split(" ");

                                    foreach (string word in words)
                                    {
                                        FilterDefinition<Record> applicantFilterDefinition = builder.Regex(property, BsonRegularExpression.Create($"/{word}/i"));
                                        containsFilterDefinitions.Add(applicantFilterDefinition);
                                    }
                                }
                                else
                                {
                                    FilterDefinition<Record> applicantFilterDefinition = builder.Regex(property, BsonRegularExpression.Create($"/{request.Values.First()}/i"));
                                    containsFilterDefinitions.Add(applicantFilterDefinition);
                                }
                            }
                        }
                        else
                        {
                            FilterDefinition<Record> applicantFilterDefinition = builder.Regex(property, BsonRegularExpression.Create($"/{request.Values.First()}/i"));
                            containsFilterDefinitions.Add(applicantFilterDefinition);
                        }
                    }
                    else if (theOperator == "IsEqualTo")
                    {
                        string value = request.Values.First();

                        // check if value can be int or else its double and parse it
                        FilterDefinition<Record> equalFilterDefinition = builder.Eq(request.Property, int.TryParse(value, out int parseResult) ? parseResult : double.Parse(value));
                        containsFilterDefinitions.Add(equalFilterDefinition);
                    }
                }

                FilterDefinition<Record> inOrFilterDefinition = builder.Or(FilterDefinition<Record>.Empty);
                FilterDefinition<Record> containsOrFilterDefinition = builder.Or(FilterDefinition<Record>.Empty);
                FilterDefinition<Record> createdByFilterDefinition = builder.Where(p => p.CreatedByUniqueID == uniqueId);

                if (inFilterDefinitions.Count() > 0)
                {
                    inOrFilterDefinition = builder.Or(inFilterDefinitions);
                }

                if (containsFilterDefinitions.Count() > 0)
                {
                    containsOrFilterDefinition = builder.Or(containsFilterDefinitions);
                }

                FilterDefinition<Record> mainFilterDefinition = builder.And(createdByFilterDefinition, inOrFilterDefinition, containsOrFilterDefinition);

                result.RecordsCount = await _recordRepository.BaseQuery().CountDocumentsAsync(mainFilterDefinition);
                baseQuery = _recordRepository.BaseQuery().Find(mainFilterDefinition);
            }

            if (queryRequest.SortRequests != null && queryRequest.SortRequests.Count() != 0)
            {
                SortDefinitionBuilder<Record> sortDefinitionBuilder = Builders<Record>.Sort;
                List<SortDefinition<Record>> sortDefinitions = new List<SortDefinition<Record>>();

                foreach (SortRequest sortRequest in queryRequest.SortRequests)
                {
                    string field = sortRequest.Field;
                    string direction = sortRequest.Direction;

                    if (field.Contains("&&"))
                    {
                        IEnumerable<string> fields = field.Split("&&");

                        foreach (string splitedField in fields)
                        {
                            field = splitedField;
                            AddSortBuilder();
                        }
                    }
                    else
                    {
                        AddSortBuilder();
                    }

                    void AddSortBuilder()
                    {
                        if (direction == "Ascending")
                        {
                            SortDefinition<Record> sortDefinition = sortDefinitionBuilder.Ascending(field);
                            sortDefinitions.Add(sortDefinition);
                        }
                        else
                        {
                            SortDefinition<Record> sortDefinition = sortDefinitionBuilder.Descending(field);
                            sortDefinitions.Add(sortDefinition);
                        }
                    }
                }
                SortDefinition<Record> mainSortDefinition = sortDefinitionBuilder.Combine(sortDefinitions);
                baseQuery = baseQuery.Sort(mainSortDefinition);
            }

            if (queryRequest.ProjectionRequest != null)
            {
                IEnumerable<string> fields = queryRequest.ProjectionRequest.ProjectionFields;
                ProjectionDefinition<Record> projection = Builders<Record>.Projection.Include(fields.First());

                foreach (string field in fields.Skip(1))
                {
                    projection = projection.Include(field);
                }
                baseQuery = baseQuery.Project<Record>(projection);
            }

            result.Records = await baseQuery.Limit(queryRequest.Limit).Skip(queryRequest.Skip).ToListAsync();

            return result;
        }

        //public async Task<IEnumerable<GridRecord>> ListGridRecordsAsync(QueryRequest queryRequest)
        //{
        //    IFindFluent<GridRecord, GridRecord> baseQuery = _gridRecordRepository.BaseQuery().Find(record => true);
        //    if (queryRequest.FilterRequests != null)
        //    {
        //        FilterDefinitionBuilder<GridRecord> builder = Builders<GridRecord>.Filter;
        //        List<FilterDefinition<GridRecord>> inFilterDefinitions = new List<FilterDefinition<GridRecord>>();
        //        List<FilterDefinition<GridRecord>> containsFilterDefinitions = new List<FilterDefinition<GridRecord>>();

        //        foreach (FilterRequest request in queryRequest.FilterRequests)
        //        {
        //            string property = request.Property;
        //            string operatorr = request.Operator;

        //            if (operatorr == "In")
        //            {
        //                FilterDefinition<GridRecord> filterDefinition = builder.In(request.Property, request.Values);
        //                inFilterDefinitions.Add(filterDefinition);
        //            }
        //            else if (operatorr == "Contains")
        //            {
        //                FilterDefinition<GridRecord> filterDefinition = builder.Regex(request.Property, BsonRegularExpression.Create($"/{request.Values.First()}/i"));
        //                containsFilterDefinitions.Add(filterDefinition);
        //            }
        //            //else if (operatorr == "Equal")
        //            //{
        //            //    FilterDefinition<GridRecord> filterDefinition = builder.Eq(request.Property, new Int16.);
        //            //    containsFilterDefinitions.Add(filterDefinition);
        //            //}
        //        }

        //        FilterDefinition<GridRecord> inOrFilterDefinition = builder.Or(FilterDefinition<GridRecord>.Empty);
        //        FilterDefinition<GridRecord> containsOrFilterDefinition = builder.Or(FilterDefinition<GridRecord>.Empty);

        //        if (inFilterDefinitions.Count() > 0)
        //        {
        //            inOrFilterDefinition = builder.Or(inFilterDefinitions);
        //        }

        //        if (containsFilterDefinitions.Count() > 0)
        //        {
        //            containsOrFilterDefinition = builder.Or(containsFilterDefinitions);
        //        }

        //        FilterDefinition<GridRecord> mainFilterDefinition = builder.And(inOrFilterDefinition, containsOrFilterDefinition);

        //        baseQuery = _gridRecordRepository.BaseQuery().Find(mainFilterDefinition);
        //    }

        //    if (queryRequest.SortRequests != null)
        //    {
        //        SortDefinitionBuilder<GridRecord> sortDefinitionBuilder = Builders<GridRecord>.Sort;
        //        List<SortDefinition<GridRecord>> sortDefinitions = new List<SortDefinition<GridRecord>>();

        //        foreach (SortRequest sortRequest in queryRequest.SortRequests)
        //        {
        //            string field = sortRequest.Field;
        //            string direction = sortRequest.Direction;
        //            if (direction == "asc")
        //            {
        //                SortDefinition<GridRecord> sortDefinition = sortDefinitionBuilder.Ascending(field);
        //                sortDefinitions.Add(sortDefinition);
        //            }
        //            else
        //            {
        //                SortDefinition<GridRecord> sortDefinition = sortDefinitionBuilder.Descending(field);
        //                sortDefinitions.Add(sortDefinition);
        //            }
        //        }
        //        SortDefinition<GridRecord> mainSortDefinition = sortDefinitionBuilder.Combine(sortDefinitions);
        //        baseQuery = baseQuery.Sort(mainSortDefinition);
        //    }

        //    if (queryRequest.ProjectionRequest != null)
        //    {
        //        IEnumerable<string> fields = queryRequest.ProjectionRequest.ProjectionFields;
        //        ProjectionDefinition<GridRecord> projection = Builders<GridRecord>.Projection.Include(fields.First());

        //        foreach (string field in fields.Skip(1))
        //        {
        //            projection = projection.Include(field);
        //        }
        //        baseQuery = baseQuery.Project<GridRecord>(projection);
        //    }

        //    return await baseQuery.Limit(queryRequest.Limit).Skip(queryRequest.Skip).ToListAsync();
        //}
    }
}
