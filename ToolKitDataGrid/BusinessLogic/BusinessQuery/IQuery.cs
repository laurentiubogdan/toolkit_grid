﻿using System.Threading.Tasks;
using ToolKitGrid.DTO.QueryModels;

namespace ToolKitGrid.BusinessLogic.BusinessQuery
{
    public interface IQuery
    {
        Task<QueryResult> ListRecordsAsync(QueryRequest queryRequest, string uniqueId);
    }
}
