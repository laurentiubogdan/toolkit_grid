﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToolKitGrid.Domain.HelperModels;
using ToolKitGrid.DTO.Responses;
using ToolKitGrid.Repository;

namespace ToolKitGrid.BusinessLogic.BusinessService
{
    public class UserSessionService : IUserSessionService
    {
        private IUserSessionRepository _userSessionRepository;

        public UserSessionService(IUserSessionRepository userSessionRepository)
        {
            _userSessionRepository = userSessionRepository;
        }

        public async Task<UserSession> InitiateSessionAsync(string userId)
        {
            if (userId == null)
            {
                return null;
            }

            string sessionId = Guid.NewGuid().ToString();

            SessionData sessionData = new SessionData
            {
                SessionId = sessionId,
                ExpiryDate = DateTime.Now.ToUniversalTime().AddMinutes(30)
            };

            UserSession existingSession = await _userSessionRepository.GetByUserIdAsync(userId);

            if (existingSession != null)
            {
                existingSession.SessionData.Add(sessionData);

                return await _userSessionRepository.UpdateByIdAsync(existingSession.Id, existingSession);
            }
            else
            {
                UserSession sessionRequest = new UserSession();

                if (sessionRequest.SessionData == null)
                {
                    sessionRequest.SessionData = new List<SessionData>();
                }

                sessionRequest.SessionData.Add(sessionData);

                sessionRequest.UserId = userId;

                await _userSessionRepository.CreateAsync(sessionRequest);

                UserSession createdUserSession = await _userSessionRepository.GetByIdAsync(sessionId);

                return createdUserSession;
            }
        }

        public async Task<UserSession> CreateUserSessionAsync(UserSession sessionRequest)
        {
            string sessionId = Guid.NewGuid().ToString();

            SessionData sessionData = new SessionData();
            sessionData.SessionId = sessionId;
            sessionData.ExpiryDate = DateTime.Now.ToUniversalTime().AddMinutes(30);

            sessionRequest.SessionData.Add(sessionData);

            await _userSessionRepository.CreateAsync(sessionRequest);

            UserSession createdUserSession = await _userSessionRepository.GetByIdAsync(sessionId);

            return createdUserSession;
        }

        public async Task<UserSessionResponse> CheckUserSession(string sessionId)
        {
            UserSession userSession = await _userSessionRepository.GetByIdAsync(sessionId);
            UserSessionResponse sessionResponse = new UserSessionResponse();

            if (userSession != null)
            {
                sessionResponse.IsValidSession = true;
            }
            else
            {
                sessionResponse.IsValidSession = false;
                sessionResponse.Error = "Invalid session.";
            }

            return sessionResponse;
        }

        public async Task<UserSession> GetUserSession(string sessionId)
        {
            UserSession userSession = new UserSession();

            userSession = await _userSessionRepository.GetByIdAsync(sessionId);

            return userSession;
        }

        public async Task<bool> CleanSessionGarbage()
        {
            IEnumerable<UserSession> repoSessions = await _userSessionRepository.GetAllAsync();
            List<SessionData> allSessionsData = new List<SessionData>();

            List<UserSession> userSessions = repoSessions.ToList();

            repoSessions.SelectMany(rs => rs.SessionData).ToList();

            foreach (SessionData sessionData in allSessionsData)
            {
                DateTime currentDateTime = DateTime.Now.ToUniversalTime();

                int dateComparison = DateTime.Compare(currentDateTime, sessionData.ExpiryDate);

                if (dateComparison > 0)
                {
                    await EndUserSession(sessionData.SessionId);
                }
            }

            return true;
        }

        public async Task<UserSessionResponse> EndUserSession(string sessionId)
        {
            UserSession userSession = await _userSessionRepository.GetByIdAsync(sessionId);
            UserSessionResponse sessionResponse = new UserSessionResponse();

            if (userSession != null)
            {
                SessionData sessionToRemove = userSession.SessionData.Single(s => s.SessionId == sessionId);

                if (sessionToRemove != null)
                {
                    userSession.SessionData.Remove(sessionToRemove);

                    if (userSession.SessionData.Count() == 0)
                    {
                        await _userSessionRepository.DeleteByIdAsync(sessionId);
                    }
                    else
                    {
                        UserSession updatedSession = await _userSessionRepository.UpdateByIdAsync(userSession.Id, userSession);
                    }

                }

                sessionResponse.IsValidSession = true;
            }
            else
            {
                sessionResponse.IsValidSession = false;
                sessionResponse.Error = "The session was not deleted!";
            }

            return sessionResponse;
        }
    }
}
