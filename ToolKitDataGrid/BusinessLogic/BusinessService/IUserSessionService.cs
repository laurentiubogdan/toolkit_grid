﻿using System.Threading.Tasks;
using ToolKitGrid.Domain.HelperModels;
using ToolKitGrid.DTO.Responses;

namespace ToolKitGrid.BusinessLogic.BusinessService
{
    public interface IUserSessionService
    {
        Task<UserSession> InitiateSessionAsync(string userId);
        Task<UserSession> CreateUserSessionAsync(UserSession sessionRequest);
        Task<UserSessionResponse> CheckUserSession(string sessionId);
        Task<UserSession> GetUserSession(string sessionId);
        Task<UserSessionResponse> EndUserSession(string sessionId);
        Task<bool> CleanSessionGarbage();
    }
}
