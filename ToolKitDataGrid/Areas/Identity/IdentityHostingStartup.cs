﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(ToolKitGrid.Areas.Identity.IdentityHostingStartup))]
namespace ToolKitGrid.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
    }
}