﻿using ToolKitGrid.Domain.Models;
using ToolKitGrid.ViewModels.Record;

namespace ToolKitGrid.ViewModelExtensionMethods
{
    public static class RecordExtensionMethods
    {
        public static GridRecordViewModel ToGridRecordViewModel(this Record model)
        {
            return new GridRecordViewModel
            {
                Id = model.Id,
                UniqueId = model.UniqueID,
                RecordName = model.RecordName,
                Applicants = model.Person.ToArrayIfNull().ToPersonViewModel(),
                CreatedBy = model.CreatedBy,
                DateCreated = model.DateCreated,
                UpdatedBy = model.UpdatedBy,
                DateUpdated = model.DateUpdated,
                BDMName = "John Wick"
            };
        }
    }
}