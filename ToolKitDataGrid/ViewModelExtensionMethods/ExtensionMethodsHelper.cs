﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using ToolKitGrid.DTO;

namespace ToolKitGrid.ViewModelExtensionMethods
{
    public static class ExtensionMethodsHelper
    {
        public static IEnumerable<T> ToArrayIfNull<T>(this IEnumerable<T> intervalArray)
        {
            if (intervalArray == null)
            {
                return Enumerable.Empty<T>();
            }

            return intervalArray;
        }

        public static string ToPersonViewModel(this IEnumerable<dynamic> personList)
        {
            if (personList != null)
            {
                List<string> fullNames = new List<string>();

                foreach (dynamic person in personList)
                {
                    string personName = "-";
                    if (HasProperty(person, "FirstName") && HasProperty(person, "Surname"))
                    {
                        personName = string.Format("{0} {1}", person.FirstName ?? "", person.Surname ?? "");
                    }
                    fullNames.Add(personName);
                }
                return String.Join(", ", fullNames);
            }
            else
            {
                return "-";
            }
        }

        public static bool HasProperty(ExpandoObject expandoObj, string name)
        {
            return ((IDictionary<string, object>)expandoObj).ContainsKey(name);
        }

        public static string ToModelMember(this string member)
        {
            switch (member)
            {
                case "Person":
                    return "Person.FirstName&&Person.Surname";
                case "UmiResult":
                    return "Umi.UmiResult";
                case "MaxBorrowing":
                    return "Umi.MaxBorrowingCapacity";
                case "PricingRequestDate":
                    return "Pricing.RequestDate";
                case "PricingResult":
                    return "Pricing.PricingResult";
                default:
                    return member;
            }
        }

        public static CustomDate GetDateFromString(this string value)
        {
            DateTime myDate;
            string[] formats = { "dd/MM/yyyy", "d/M/yyyy", "d/MM/yyyy", "dd/M/yyyy","yyyy/M/d","yyyy/MM/d","yyyy/M/dd","yyyy/MM/dd",
                                 "dd-MM-yyyy", "d-M-yyyy", "d-MM-yyyy", "dd-M-yyyy","yyyy-M-d","yyyy-MM-d","yyyy-M-dd","yyyy-MM-dd",
                                 "dd.MM.yyyy", "d.M.yyyy", "d.MM.yyyy", "dd.M.yyyy","yyyy.M.d","yyyy.MM.d","yyyy.M.dd","yyyy.MM.dd"};
            var isDateValid = DateTime.TryParseExact(value, formats, new CultureInfo("en-GB"), DateTimeStyles.None, out myDate);

            CustomDate date = new CustomDate
            {
                Day = 0,
                Month = 0,
                Year = 0
            };

            if (isDateValid)
            {
                date.Day = myDate.Day;
                date.Month = myDate.Month;
                date.Year = myDate.Year;

                return date;
            }

            return date;
        }
    }
}
