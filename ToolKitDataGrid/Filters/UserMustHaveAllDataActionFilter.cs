﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Security.Claims;
using ToolKitGrid.Domain.Models.Identity;
using ToolKitGrid.ExtensionMethods;
using ToolKitGrid.Repository;
using Microsoft.Extensions.DependencyInjection;
using ToolKitGrid.ViewModels.Profile;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace ToolKitGrid.Filters
{
    public class UserMustHaveAllDataActionFilter : ActionFilterAttribute
    {
        public bool Disable { get; set; }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (Disable)
                return;

            object loanWriterRepository = context.HttpContext.RequestServices.GetService<ILoanWriterRepository>();

            LoanWriter loggedInLoanwriter = context.HttpContext.User.GetClaim<LoanWriter>(ClaimTypes.UserData);

            ExternalLoanWriterModel externalLoanWriterModel = loggedInLoanwriter.ToExternalLoanWriterModel();

            ValidationContext validateContext = new ValidationContext(externalLoanWriterModel, serviceProvider: null, items: null);
            List<ValidationResult> results = new List<ValidationResult>();
            bool isExternalProfileFull = Validator.TryValidateObject(externalLoanWriterModel, validateContext, results);

            if (isExternalProfileFull == false)
            {
                //redirect to a page to fill all the user data
                context.Result = new RedirectToPageResult("/UserProfile");
            }
        }
    }
}
