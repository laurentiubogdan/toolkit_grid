﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using ToolKitGrid.Domain.Models.Identity;
using ToolKitGrid.ExtensionMethods;
using ToolKitGrid.Repository;

namespace ToolKitGrid.Filters.Middlewares
{
    public class LoginWithDefaultUserMiddleware
    {
        private readonly RequestDelegate _next;
        public LoginWithDefaultUserMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, ILoanWriterRepository loanWriterRepository)
        {
            if (context.User.Identity.IsAuthenticated == false)
            {
                string defaultUserUniqueId = AppSettings.DefaultLoginUserUniqueID;

                LoanWriter loanWriter = await loanWriterRepository.GetByUniqueIdAsync(defaultUserUniqueId);

                //create Identity
                ClaimsIdentity identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
                identity.AddClaim(new Claim(ClaimTypes.Name, loanWriter.LoanWriterEmail ?? ""));
                identity.AddClaim(new Claim("ReturnURL", loanWriter.ReturnUrl));
                identity.AddClaim(ClaimTypes.UserData, loanWriter);

                //sign the identity
                ClaimsPrincipal principal = new ClaimsPrincipal(identity);
                await context.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, new AuthenticationProperties
                {
                    IsPersistent = true,
                    AllowRefresh = true
                });

                context.Response.Redirect(context.Request.Path);
                return;
            }
            await _next(context);
        }
    }
}
