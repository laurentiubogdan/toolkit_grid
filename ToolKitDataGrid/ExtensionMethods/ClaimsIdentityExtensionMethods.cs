﻿using Newtonsoft.Json;
using System.Security.Claims;

namespace ToolKitGrid.ExtensionMethods
{
    public static class ClaimsIdentityExtensionMethods
    {
        public static void AddClaim<T>(this ClaimsIdentity claimsIdentity, string claimKey, T complexValue)
        {
            claimsIdentity.AddClaim(new Claim(claimKey, JsonConvert.SerializeObject(complexValue)));
        }       
    }
}
