﻿using ToolKitGrid.Domain.HelperModels;
using ToolKitGrid.DTO.Requests.CreateRequests;

namespace ToolKitGrid.ExtensionMethods
{
    public static class UserProfileExtensionMethods
    {
        public static UserProfile CreateRequestToModel(this UserProfileCreateRequest request)
        {
            return new UserProfile
            {
                UserId = request.UserId,
                ItemsPerPage = request.ItemsPerPage,
                Columns = request.Columns,
                VisibleColumns = request.VisibleColumns
            };
        }
    }
}
