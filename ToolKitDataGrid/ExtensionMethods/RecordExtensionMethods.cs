﻿using MongoDB.Bson.Serialization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using ToolKitGrid.Domain.Models;
using ToolKitGrid.Domain.Models.Embedded;
using ToolKitGrid.Domain.Models.Identity;
using ToolKitGrid.DTO.Requests.CreateRequests;

namespace ToolKitGrid.ExtensionMethods
{
    public static class RecordExtensionMethods
    {
        public static Record CreateRequestToModel(this RecordCreateRequest request, LoanWriter loanWriter)
        {
            string recordUniqueId = CreateUniqueId("RCD-999999");

            if (recordUniqueId == "")
            {
                recordUniqueId = Guid.NewGuid().ToString();
            }

            dynamic household = BsonSerializer.Deserialize<dynamic>(request.Household);
            household.UniqueID = CreateUniqueId("HLD-999999");

            dynamic person = BsonSerializer.Deserialize<dynamic>(request.Person);
            person.UniqueID = CreateUniqueId("PER-999999");

            List<string> statusNew = new List<string>();
            statusNew.Add("New");

            return new Record
            {
                UniqueID = recordUniqueId,
                RecordName = request.Name,
                DateCreated = DateTime.Now,
                CreatedBy = loanWriter.FullName,
                CreatedByUniqueID = loanWriter.UniqueID,
                Status = statusNew,
                Household = new List<dynamic> { household },
                Person = new List<dynamic> { person },
                UpdatedBy = loanWriter.FullName,
                DateUpdated = DateTime.Now
            };
        }

        public static string CreateUniqueId(string mask)
        {
            //default mask if null
            if (mask == null)
            {
                mask = "RCD-999999";
            }

            string numbers = mask.Substring(mask.LastIndexOf("-") + 1);

            int dashLocation = mask.IndexOf("-", StringComparison.Ordinal);

            if (dashLocation > 0)
            {
                string uidPrefix = mask.Substring(0, dashLocation);
                int numbersCounter = Int32.Parse(numbers);
                int formatCounter = numbersCounter.ToString().Split("9").Length - 1;

                Random generator = new Random();
                string randomNumbers = generator.Next(0, numbersCounter).ToString("D" + formatCounter.ToString());

                string generatedUid = uidPrefix + "-" + randomNumbers;

                return generatedUid;
            }

            return "";
        }
    }
}
