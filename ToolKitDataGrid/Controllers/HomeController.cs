﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ToolKitGrid.Filters;
using ToolKitGrid.Helpers.AWS;

namespace ToolKitGrid.Controllers
{
    [Authorize]
    [UserMustHaveAllDataActionFilter]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View("Pages/Index.cshtml");
        }
    }
}