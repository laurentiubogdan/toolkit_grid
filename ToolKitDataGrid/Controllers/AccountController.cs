﻿using Elmah.Io.AspNetCore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ToolKitGrid.Configuration;
using ToolKitGrid.Domain.Models.Identity;
using ToolKitGrid.ExtensionMethods;
using ToolKitGrid.Filters;
using ToolKitGrid.Repository;
using ToolKitGrid.Services;
using ToolKitGrid.Services.Jwt;
using ToolKitGrid.ViewModels.Profile;

namespace ToolKitGrid.Controllers
{
    [AllowAnonymous]
    [UserMustHaveAllDataActionFilter(Disable = true)]
    public class AccountController : Controller
    {
        private readonly IJwtService _jwtService;
        private readonly ILoanWriterRepository _loanWriterRepository;
        private readonly IConfiguration _configuration;

        public AccountController(IJwtService jwtService,
            ILoanWriterRepository loanWriterRepository,
            IConfiguration configuration)
        {
            _jwtService = jwtService;
            _loanWriterRepository = loanWriterRepository;
            _configuration = configuration;
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return Unauthorized();
        }

        public async Task<IActionResult> BackToOrigin()
        {
            string returnUrl = User.GetClaim("ReturnURL").Value;
            await HttpContext.SignOutAsync();
            return Redirect(returnUrl);
        }

        public async Task<IActionResult> Login(string token)
        {
            string jwt = token;

            if (string.IsNullOrEmpty(token))
            {
                if (HttpContext.User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                await HttpContext.SignOutAsync();
            }

            //validate jwt
            string jwtValidationFailMessage = default(string);
            ValidateJwtResponse validateJwtResponse = null;
            try
            {
                validateJwtResponse = await _jwtService.ValidateAsync(jwt);
            }
            catch (Exception ex)
            {
                jwtValidationFailMessage = ex.Message;
                ex.Ship(HttpContext);
                return Unauthorized();
            }

            if (validateJwtResponse.Valid == true)
            {
                //Get session data for the user mentioned in the JWT payload. 
                string oneTimeUseToken = _jwtService.GetJwtPayload(jwt)[_configuration[Constants.OneTimeUseUserTokenPayloadJwtKey]].ToString();
                LoanWriter loanWriter = await _loanWriterRepository.GetUserByOneTimeUseTokenAsync(oneTimeUseToken);

                if (loanWriter == null)
                {
                    return Unauthorized();
                }
                //TODO: uncomment on production
                loanWriter.OneTimeUseToken = null;
                await _loanWriterRepository.UpdateByIdAsync(loanWriter.UniqueID, loanWriter);

                await SignInAsync(loanWriter);

                return RedirectToAction("Index", "Home");
            }

            return Unauthorized();
        }

        private async Task SignInAsync(LoanWriter loanWriter)
        {
            //create Identity
            ClaimsIdentity identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
            identity.AddClaim(new Claim(ClaimTypes.Name, loanWriter.LoanWriterEmail ?? ""));
            identity.AddClaim(new Claim("ReturnURL", loanWriter.ReturnUrl));
            identity.AddClaim(ClaimTypes.UserData, loanWriter);

            //sign the identity
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, new AuthenticationProperties
            {
                IsPersistent = true,
                AllowRefresh = true
            });
        }

        //Has to be moved under a secured controller. 
        public JsonResult GetUserProfile()
        {
            LoanWriter loanWriter = HttpContext.User.GetClaim<LoanWriter>(ClaimTypes.UserData);

            ExternalLoanWriterModel externalLoanWriterModel = loanWriter.ToExternalLoanWriterModel();

            IList<string> lockedFieldsNames = ExternalLoanWriterMapService.GetLockedFields(externalLoanWriterModel);

            UserProfileJsonForm userProfileJsonForm = new UserProfileJsonForm
            {
                ExternalLoanWriterModel = externalLoanWriterModel,
                LockedFields = lockedFieldsNames.ToArray()
            };

            return new JsonResult(userProfileJsonForm);
        }

        //Has to be moved under a secured controller. 
        [HttpPost]
        public async Task<IActionResult> UpdateProfile(ExternalLoanWriterModel externalLoanWriterModel)
        {
            //get current user 
            LoanWriter loanWriter = await _loanWriterRepository.GetByUniqueIdAsync(externalLoanWriterModel.UniqueID);

            if (string.IsNullOrEmpty(externalLoanWriterModel.UniqueID))
            {
                return new JsonResult(new
                {
                    success = true
                });
            }

            //map external userprofile on saved user
            LoanWriterMapService.MapExternalOnCurrent(externalLoanWriterModel, loanWriter);

            //update db 
            await _loanWriterRepository.UpdateByIdAsync(loanWriter.UniqueID, loanWriter);

            //reset claim
            await HttpContext.SignOutAsync();
            await SignInAsync(loanWriter);

            return new JsonResult(new
            {
                success = true
            });
        }
    }
}