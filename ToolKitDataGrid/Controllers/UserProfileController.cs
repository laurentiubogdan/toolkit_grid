﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToolKitGrid.BusinessLogic.BusinessQuery;
using ToolKitGrid.Domain.HelperModels;
using ToolKitGrid.DTO.Requests.CreateRequests;
using ToolKitGrid.ExtensionMethods;
using ToolKitGrid.Repository;

namespace ToolKitGrid.Controllers
{
    [Route("[controller]/[action]")]
    [Authorize]
    public class UserProfileController : ControllerBase
    {
        private IUserProfileRepository _userProfileRepository;
        private IQuery _query;

        public UserProfileController(IUserProfileRepository userProfileRepository, IQuery query)
        {
            _userProfileRepository = userProfileRepository;
            _query = query;
        }

        [HttpPost]
        public async Task<IActionResult> GetAllUserProfiles()
        {
            IEnumerable<UserProfile> userProfiles = await _userProfileRepository.GetAllAsync();

            if (userProfiles == null)
            {
                return NotFound("No user profiles available");
            }

            return Ok(userProfiles);
        }

        [HttpPost]
        public async Task<IActionResult> GetUserProfilesByUserId(string userId)
        {
            UserProfile userProfile = await _userProfileRepository.GetByUserIdAsync(userId);

            if (userProfile == null)
            {
                return NoContent();
            }

            return Ok(userProfile);
        }

        [HttpPost]
        public async Task<IActionResult> CreateUserProfile(UserProfileCreateRequest request)
        {
            UserProfile profile = request.CreateRequestToModel();

            UserProfile userProfile = await _userProfileRepository.CreateAsync(profile);


            return Ok(userProfile);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateUserProfile(UserProfile profile)
        {
            UserProfile userProfile = await _userProfileRepository.GetByIdAsync(profile.Id);

            if (userProfile == null)
            {
                return NotFound("No user profile available");
            }

            UserProfile updatedUserProfile = await _userProfileRepository.UpdateByIdAsync(profile.Id, profile);

            return Ok(updatedUserProfile);
        }
    }
}