﻿using AuthorizeOpen.Api;
using JwtGeneratorApi.Api;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ToolKitGrid.BusinessLogic.BusinessQuery;
using ToolKitGrid.BusinessLogic.BusinessService;
using ToolKitGrid.Configuration;
using ToolKitGrid.Domain.HelperModels;
using ToolKitGrid.Domain.Models;
using ToolKitGrid.Domain.Models.Identity;
using ToolKitGrid.DTO.QueryModels;
using ToolKitGrid.DTO.Requests.ArchiveRequests;
using ToolKitGrid.DTO.Requests.CloneRequests;
using ToolKitGrid.DTO.Requests.CreateRequests;
using ToolKitGrid.DTO.Requests.DeleteRequests;
using ToolKitGrid.DTO.Responses;
using ToolKitGrid.ExtensionMethods;
using ToolKitGrid.Filters;
using ToolKitGrid.HelpersController.Record;
using ToolKitGrid.JWTApi;
using ToolKitGrid.Repository;
using ToolKitGrid.ViewModelExtensionMethods;

namespace ToolKitGrid.Controllers
{
    [Authorize]
    [UserMustHaveAllDataActionFilter]
    [Route("[controller]/[action]")]
    public class RecordController : Controller
    {
        private readonly UserApiMethods _userApiMethods;
        private IRecordRepository _recordRepository;
        private IQuery _query;
        private IUserSessionService _userSessionService;
        private IRecordControllerHelper _controllerHelper;
        private IAuthorizeOpenApi _authorizeOpenApi;
        private ILoanWriterRepository _loanWriterRepository;
        private IJwtApi _jwtApi;
        private IConfiguration _configuration;

        public RecordController(
            UserApiMethods userApiMethods,
            IRecordRepository recordRepository,
            IQuery query,
            IUserSessionService userSessionRepository,
            IRecordControllerHelper controllerHelper,
            IAuthorizeOpenApi authorizeOpenApi,
            IJwtApi jwtApi,
            ILoanWriterRepository loanWriterRepository,
            IConfiguration configuration)
        {
            _userApiMethods = userApiMethods;
            _recordRepository = recordRepository;
            _query = query;
            _userSessionService = userSessionRepository;
            _controllerHelper = controllerHelper;
            _authorizeOpenApi = authorizeOpenApi;
            _jwtApi = jwtApi;
            _loanWriterRepository = loanWriterRepository;
            _configuration = configuration;
        }

        [HttpGet("record/list")]
        public async Task<IActionResult> GetAllRecordsAsync(string sessionId)
        {
            UserSession userSession = await _userSessionService.GetUserSession(sessionId);

            if (userSession == null)
            {
                return BadRequest();
            }

            IEnumerable<Record> records = await _recordRepository.GetByUserIdAsync(sessionId);

            if (records == null)
            {
                return NotFound("No Records available");
            }

            return Ok(records);
        }

        [HttpPost]
        public async Task<IActionResult> GridListRecords([DataSourceRequest] DataSourceRequest request)
        {
            LoanWriter loanWriter = HttpContext.User.GetClaim<LoanWriter>(ClaimTypes.UserData);
            QueryRequest queryRequest = new QueryRequest
            {
                FilterRequests = new List<FilterRequest>(),
                SortRequests = new List<SortRequest>()
            };

            ModifyFilters(request.Filters);

            void ModifyFilters(IEnumerable<IFilterDescriptor> filters)
            {
                if (filters.Any())
                {
                    foreach (IFilterDescriptor filter in filters)
                    {
                        FilterDescriptor descriptor = filter as FilterDescriptor;
                        if (descriptor != null)
                        {
                            descriptor.Member = descriptor.Member.ToModelMember();

                            FilterRequest filterRequest = new FilterRequest
                            {
                                Property = descriptor.Member,
                                Operator = descriptor.Operator.ToString(),
                                Values = new List<string>() { descriptor.Value.ToString() }
                            };
                            queryRequest.FilterRequests.Add(filterRequest);
                        }
                        else if (filter is CompositeFilterDescriptor)
                        {
                            ModifyFilters(((CompositeFilterDescriptor)filter).FilterDescriptors);
                        }
                    }
                }
            }

            var kendoSorts = request.Sorts;

            foreach (SortDescriptor sort in kendoSorts)
            {
                sort.Member = sort.Member.ToModelMember();

                queryRequest.SortRequests.Add(new SortRequest
                {
                    Field = sort.Member,
                    Direction = sort.SortDirection.ToString()
                });
            }

            queryRequest.Limit = request.PageSize;
            queryRequest.Skip = request.PageSize * (request.Page - 1);

            QueryResult queryResult = await _query.ListRecordsAsync(queryRequest, loanWriter.UniqueID);
            GridResponse gridResponse = new GridResponse
            {
                RecordsCount = queryResult.RecordsCount,
                GridRecords = queryResult.Records.Select(p => p.ToGridRecordViewModel())
            };

            return Ok(gridResponse);
        }

        [HttpGet]
        public async Task<IActionResult> GetRecordByUniqueIdAsync(string uniqueId)
        {
            Record record = await _recordRepository.GetByUniqueIdAsync(uniqueId);

            if (record == null)
            {
                return NotFound("Record wasn't found");
            }

            return Ok(record);
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync(RecordCreateRequest recordCreateRequest)
        {
            LoanWriter loanWriter = HttpContext.User.GetClaim<LoanWriter>(ClaimTypes.UserData);
            Record record = recordCreateRequest.CreateRequestToModel(loanWriter);

            await _recordRepository.CreateAsync(record);

            if (record == null)
            {
                return BadRequest();
            }

            OpenRecordRequest request = new OpenRecordRequest();
            request.RecordUniqueId = record.UniqueID;

            //Uncomment to use old way to generate open URL
            SendJwtResult result = await _userApiMethods.SendJWTAsync(request); //todo to replace this with new generate of the URL
            return Ok(result);

            //New implementation of the generating open URL
            //Comment this if you don't to use new way to generate signed URL
            //string signedOpenUrl = await this.GenerateAuthorizedOpenUrlAsync(record.UniqueID);

            //return Ok(new
            //{
            //    Success = true,
            //    Url = signedOpenUrl,
            //});
        }

        [HttpPost]
        public async Task<IActionResult> CloneAsync(CloneRequest request)
        {
            Record record = await _recordRepository.GetByUniqueIdAsync(request.UniqueID);
            record.Id = null;
            record.RecordName = request.RecordName;
            record.UniqueID = Guid.NewGuid().ToString();
            record.DateCreated = DateTime.Now;
            await _recordRepository.CreateAsync(record);

            if (record == null)
            {
                return BadRequest();
            }

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> ArchiveAsync(ArchiveRecordRequest request)
        {
            await _recordRepository.UpdateManyAsync(request.Ids, request.Archive);

            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> UpdateAsync(string uniqueId, [FromBody]Record request)
        {
            LoanWriter loanWriter = HttpContext.User.GetClaim<LoanWriter>(ClaimTypes.UserData);
            request.UpdatedBy = loanWriter.FullName;
            Record updatedRecord = await _recordRepository.UpdateByIdAsync(uniqueId, request);

            if (updatedRecord == null)
            {
                return BadRequest();
            }

            return Ok(updatedRecord);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteAsync(string id)
        {
            await _recordRepository.DeleteByIdAsync(id);

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> DeleteManyAsync(DeleteRecordRequest request)
        {
            await _recordRepository.DeleteManyAsync(request.Ids);

            return Ok();
        }

        [HttpPost]
        public JsonResult GetUserSession()
        {
            LoanWriter user = HttpContext.User.GetClaim<LoanWriter>(ClaimTypes.UserData);

            return new JsonResult(new
            {
                UserId = user.UniqueID
            });
        }

        [HttpPost]
        public JsonResult RenderPartialContent(string rootToPartial, dynamic model)
        {
            return new JsonResult(new
            {
                view = _controllerHelper.RenderRazorViewToString(this, rootToPartial, null)
            });
        }

        [HttpPost]
        public async Task<IActionResult> SendRecordToQualify(string uniqueId)
        {
            Record record = await _recordRepository.GetByUniqueIdAsync(uniqueId);
            if (record == null)
            {
                return BadRequest();
            }

            OpenRecordRequest request = new OpenRecordRequest();

            request.RecordUniqueId = record.UniqueID;

            //Uncomment to use old way to generate open URL
            SendJwtResult result = await _userApiMethods.SendJWTAsync(request); //todo to replace this with new generate of the URL
            return Ok(result);

            //New implementation of the generating open URL
            string signedUrl = null;
            try
            {
                signedUrl = await this.GenerateAuthorizedOpenUrlAsync(uniqueId);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false
                });
            }

            return Ok(new
            {
                Success = true,
                Url = signedUrl
            });
        }

        private async Task<string> GenerateAuthorizedOpenUrlAsync(string recordId)
        {
            //new implementation of the generating open URL 
            string oneTimeUseToken = Guid.NewGuid().ToString();
            GenerateJwtResponse generateJwtResponse = await _jwtApi.GenerateAsync(new GenerateJwtRequest
            {
                Subject = Constants.GridApplicationName,
                SecretKeyId = Constants.GridApplicationSecretId,
                Seconds = 30
            });

            LoanWriter claimLoanWriter = HttpContext.User.GetClaim<LoanWriter>(ClaimTypes.UserData);
            LoanWriter loanWriter = await _loanWriterRepository.GetByUniqueIdAsync(claimLoanWriter.UniqueID);
            loanWriter.ReturnUrl = _configuration[Constants.GridQualifyUrlKey];

            GenerateOpenUrlResponse generateOpenUrlResponse = await _authorizeOpenApi.GenerateOpenUrlAsync(new GenerateOpenUrlRequest
            {
                AuthorizationToken = generateJwtResponse.Jwt,
                Payload = new GenerateOpenUrlBody
                {
                    User = loanWriter.ToAuthorizeOpenUser(),
                    ReturnUrl = loanWriter.ReturnUrl
                }
            });

            //Local testing URL: 
            //Local testing
            //if (AppSettings.SecurityEnabled == false)
            //{
            //    string tempToken = generateOpenUrlResponse.Url.Substring(generateOpenUrlResponse.Url.IndexOf("=") + 1);
            //    generateOpenUrlResponse.Url = $"https://localhost:44363/view/{recordId}?token={tempToken}";
            //}

            return $"{generateOpenUrlResponse.Url}";
        }
    }
}