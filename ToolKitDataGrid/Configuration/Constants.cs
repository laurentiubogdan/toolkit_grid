﻿namespace ToolKitGrid.Configuration
{
    internal class Constants
    {
        internal const string AuthorizeLambdaFunctionNameConfigKey = "AuthorizeLambdaFunctionName";
        internal const string OneTimeUseUserTokenPayloadJwtKey = "OneTimeUseUserTokenPayloadJwtKey";
        internal const string AuthSessionIdConfigKey = "AuthSessionId";
        internal const string ApplySecurityConfigKey = "SecurityEnabled";
        internal const string DefaultLoginUserUniqueIDCoinfigKey = "DefaultLoginUserUniqueID";
        internal static string AwsAccessKey = "AWS:AccessKey";
        internal static string AwsSecretKey = "AWS:SecretKey";

        internal static string PublicJsonS3Versions = "PublicJsonS3Versions";
        internal static string EnvironmentBucket = "EnvironmentBucket";
        internal static string AwsRegionEndpointKey = "AWS:Region";
        internal static string GridApplicationName = "simpologygrid";
        internal static string GridApplicationSecretId = "QualifyGridClient";
        internal static string AuthorizeOpenUrlKey = "AuthorizeOpenUrl";
        internal static string JwtGeneratorApiLambdaFunctionNameKey = "JwtGeneratorApiLambdaFunctionName";
        internal static string GridQualifyUrlKey = "GridQualifyUrl";
        
    }
}
