using Amazon;
using Amazon.Lambda;
using AuthorizeOpen.Api;
using Elmah.Io.AspNetCore;
using Jdenticon.AspNetCore;
using JwtGeneratorApi.Api;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using System;
using System.Net.Http;
using ToolkitGrid.Repository.Mongo;
using ToolKitGrid.BusinessLogic.BusinessQuery;
using ToolKitGrid.BusinessLogic.BusinessService;
using ToolKitGrid.Configuration;
using ToolKitGrid.Domain;
using ToolKitGrid.Filters;
using ToolKitGrid.Filters.Middlewares;
using ToolKitGrid.Helpers.AWS;
using ToolKitGrid.HelpersController.Record;
using ToolKitGrid.JWTApi;
using ToolKitGrid.JWTApi.Helpers;
using ToolKitGrid.Repository;
using ToolKitGrid.Services.Jwt;

namespace ToolKitGrid
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            AppSettings.Configuration = configuration;
            AppSettings.Environment = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
            });
            services.Configure<ConnectionStrings>(Configuration.GetSection("ConnectionStrings"));
            services.AddScoped(p => new UriApiMenthodsConfiguration { RecordQualifyUrl = this.Configuration.GetValue<string>("RecordQualifyUrl") });
            services.AddSingleton<MongoUnitOfWork>();
            services.AddScoped<UserApiMethods>();
            services.AddScoped<IRecordRepository, RecordRepository>();
            services.AddScoped<IUserSessionRepository, UserSessionRepository>();
            services.AddScoped<IUserProfileRepository, UserProfileRepository>();
            services.AddScoped<ILoanWriterRepository, LoanWriterRepository>();
            services.AddScoped<IQuery, Query>();
            services.AddScoped<IUserSessionService, UserSessionService>();
            services.AddScoped<IRecordControllerHelper, RecordControllerHelper>();
            services.AddScoped<IJwtService, JwtService>();
            services.AddSingleton(p => new JwtApiConfiguration
            {
                AwsKey = Configuration[Constants.AwsAccessKey],
                AwsSecret = Configuration[Constants.AwsSecretKey],
                RegionEndpoint  = RegionEndpoint.GetBySystemName(Configuration[Constants.AwsRegionEndpointKey]),
                LambdaFunctionArn = Configuration[Constants.JwtGeneratorApiLambdaFunctionNameKey]
            });
            services.AddScoped<IJwtApi, JwtApi>();
            services.AddSingleton(p => new AuthorizeOpenConfigurations
            {
                ApiUrl = Configuration[Constants.AuthorizeOpenUrlKey]
            });
            services.AddScoped<IAuthorizeOpenApi, AuthorizeOpenApi>();
            services.AddScoped<UserMustHaveAllDataActionFilter>();
            services.AddSingleton<HttpClient>();
            services.AddAWSService<IAmazonLambda>();
            services.AddScoped<IAwsConfigurationFactory, AwsConfigurationFactory>();

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddCors();

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options =>
                                    options.SerializerSettings.ContractResolver = new DefaultContractResolver());

            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                options.Cookie.Name = "User.Cookie";
                options.IdleTimeout = TimeSpan.FromMinutes(60);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            }).AddCookie(options =>
            {
                options.LoginPath = "/account/login";
                options.LogoutPath = "/account/logout";
            });

            services.AddHttpClient();
            services.AddKendo();

            //elmah
            services.Configure<ElmahIoOptions>(Configuration.GetSection("ElmahIo"));
            services.AddElmahIo();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider services)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseStatusCodePages();
                app.UseHsts();
            }

            //elmah
            app.UseElmahIo();

            app.UseSession();
            app.UseHttpsRedirection();
            app.UseJdenticon();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();

            //middleware
            #region Middlewares
            if (AppSettings.SecurityEnabled == false)
            {
                app.UseDefaultUser();
            }
            #endregion

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

        }
    }
}
