﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ToolKitGrid.HelpersController.Record
{
    public interface IRecordControllerHelper
    {
        Task<string> RenderRazorViewToString(Controller controller, string partialView, object model);
        Task<string> GetViewPageHtml(Controller controller, string viewName, object model);
    }
}
